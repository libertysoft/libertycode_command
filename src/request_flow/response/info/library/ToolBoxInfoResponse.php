<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\command\request_flow\response\info\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\route\route\api\RouteInterface;
use liberty_code\route\route\api\RouteCollectionInterface;
use liberty_code\request_flow\response\model\DefaultResponse;
use liberty_code\command\route\library\ToolBoxCommandRoute;
use liberty_code\command\route\library\ToolBoxInfoRoute;
use liberty_code\command\request_flow\front\model\CommandFrontController;



class ToolBoxInfoResponse extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get associative array of routes from specified routes list.
     * Format: key = route key => value = route object
     *
     * @param RouteInterface[]|RouteCollectionInterface $routes
     * @return null|RouteInterface[]
     */
    protected static function getTabRoute($routes)
    {
        // Init var
        $result = null;

        // Get array of routes, in array case
        if(is_array($routes))
        {
            foreach($routes as $objRoute)
            {
                if($objRoute instanceof RouteInterface)
                {
                    // Init result, if required
                    if(is_null($result))
                    {
                        $result = array();
                    }

                    /** @var RouteInterface $objRoute */
                    $result[$objRoute->getStrKey()] = $objRoute;
                }
            }
        }
        // Get array of routes, in collection case
        else if($routes instanceof RouteCollectionInterface)
        {
            foreach($routes->getTabKey() as $strKey)
            {
                // Init result, if required
                if(is_null($result))
                {
                    $result = array();
                }

                $result[$strKey] = $routes->getObjRoute($strKey);
            }
        }

        // Return result
        return $result;
    }



    /**
     * Get string table from specified data (2 dimension array of stringable values).
     *
     * @param array $tabData
     * @param string $strFormatCellFirst = '%1$s        '
     * @param string $strFormatCellLast = '%1$s        '
     * @param string $strFormatCell = '%1$s        '
     * @param string $strFormatRowFirst = '%1$s'
     * @param string $strFormatRowLast = '%1$s'
     * @param string $strFormatRow = PHP_EOL . '%1$s'
     * @return null|string
     */
    public static function getStrTable(
        array $tabData,
        $strFormatCellFirst = '%1$s        ',
        $strFormatCellLast = '%1$s        ',
        $strFormatCell = '%1$s        ',
        $strFormatRowFirst = '%1$s',
        $strFormatRowLast = PHP_EOL . '%1$s',
        $strFormatRow = PHP_EOL . '%1$s'
    )
    {
        // Init var
        $result = null;
        $tabSize = array();

        // Check data is valid and get maximum size for each column
        foreach($tabData as $rowData) // Run all rows
        {
            // Check row is valid
            if((!is_array($rowData)) || (count($rowData) == 0))
            {
                $tabSize = array();
                break;
            }

            // Run all columns
            $intCpt = 0;
            foreach($rowData as $data)
            {
                // Check column is valid
                if((!is_string($data)) && (!is_numeric($data)) && (!is_bool($data)))
                {
                    $tabSize = array();
                    break;
                }

                // Register size if required
                $intSize = mb_strlen(strval($data));
                if((!isset($tabSize[$intCpt])) || ($tabSize[$intCpt] < $intSize) )
                {
                    $tabSize[$intCpt] = $intSize;
                }

                $intCpt++;
            }
        }

        // Build string table if data valid (maximum column sizes found)
        if(count($tabSize) > 0)
        {
            // Run all rows
            $intCptRow = 0;
            foreach($tabData as $rowData)
            {
                // Run all columns
                $intCpt = 0;
                $strRow = '';
                foreach($rowData as $data)
                {
                    // Get info
                    $intSize = $tabSize[$intCpt];
                    $data = str_pad(strval($data), $intSize);

                    // Build string row
                    if($intCpt == 0) // In first case
                    {
                        $strRow = sprintf($strFormatCellFirst, $data);
                    }
                    // In last case
                    else if($intCpt == (count($rowData) - 1))
                    {
                        $strRow .= sprintf($strFormatCellLast, $data);
                    }
                    // In standard case
                    else
                    {
                        $strRow .= sprintf($strFormatCell, $data);
                    }


                    $intCpt++;
                }

                // Build string result
                if(is_null($result)) // In first case
                {
                    $result = sprintf($strFormatRowFirst, $strRow);
                }
                // In last case
                else if((count($tabData) - 1) == $intCptRow)
                {
                    $result .= sprintf($strFormatRowLast, $strRow);
                }
                // In standard case
                else
                {
                    $result .= sprintf($strFormatRow, $strRow);
                }

                $intCptRow++;
            }
        }

        // Return result
        return $result;
    }





    // Methods getters command content
    // ******************************************************************************

    /**
     * Get string command information about specified route.
     *
     * @param RouteInterface $objRoute
     * @return null|string
     */
    protected static function getStrCommandContentRoute(RouteInterface $objRoute)
    {
        // Init var
        $result = null;
        $strFormatDescArg =
            'Route "%1$s":' . PHP_EOL .
            '%2$s' . PHP_EOL .
            PHP_EOL .
            'Arguments: ' . PHP_EOL .
            '%3$s' . PHP_EOL;
        $strFormatDesc =
            'Route "%1$s":' . PHP_EOL .
            '%2$s' . PHP_EOL;
        $strFormatArg =
            'Route "%1$s" - Arguments:' . PHP_EOL .
            '%2$s' . PHP_EOL;

        // Get info
        $strKey = $objRoute->getStrKey();
        $strDesc = ToolBoxInfoRoute::getStrRouteDesc($objRoute);
        $strSrcArg = ToolBoxInfoRoute::getStrRouteSrcArgInfo($objRoute);

        // Build info, if required
        if((!is_null($strDesc)) && (!is_null($strSrcArg)))
        {
            $result = sprintf($strFormatDescArg, $strKey, $strDesc, $strSrcArg);
        }
        else if(!is_null($strDesc))
        {
            $result = sprintf($strFormatDesc, $strKey, $strDesc);
        }
        else if(!is_null($strSrcArg))
        {
            $result = sprintf($strFormatArg, $strKey, $strSrcArg);
        }

        // Return result
        return $result;
    }



    /**
     * Get string command information about specified routes list.
     *
     * @param RouteInterface[]|RouteCollectionInterface $routes
     * @return null|string
     */
    public static function getStrCommandContentRouteList($routes)
    {
        // Init var
        $result = null;
        $tabRoute = static::getTabRoute($routes);
        $strLink = PHP_EOL . PHP_EOL;

        // Check array of routes is valid
        if(!is_null($tabRoute))
        {
            // Run all routes
            foreach($tabRoute as $objRoute)
            {
                // Build info if route command info valid
                $strInfo = static::getStrCommandContentRoute($objRoute);
                if(!is_null($strInfo))
                {
                    if(is_null($result))
                    {
                        $result = $strInfo;
                    }
                    else
                    {
                        $result = $result && $strLink && $strInfo;
                    }
                }
            }
        }

        // Return result
        return $result;
    }



    /**
     * Get string command information from specified string command and specified route collection.
     *
     * @param RouteCollectionInterface $objRouteCollection
     * @param string $strSrc
     * @return null|string
     */
    public static function getStrCommandContent(RouteCollectionInterface $objRouteCollection, $strSrc)
    {
        // Init var
        $tabRoute = $objRouteCollection->getTabRoute($strSrc);
        $result = static::getStrCommandContentRouteList($tabRoute);

        // Return result
        return $result;
    }





    // Methods getters summary content
    // ******************************************************************************

    /**
     * Get string summary information from specified routes list.
     *
     * @param RouteInterface[]|RouteCollectionInterface $routes
     * @return null|string
     */
    public static function getStrSummaryContent($routes)
    {
        // Init var
        $result = null;
        $tabRoute = static::getTabRoute($routes);
        $strFormatSrcWithRoute = '%1$s (%2$s)';

        // Check array of routes is valid, and build array of commands info
        $tabInfoCmd = array();
        if(!is_null($tabRoute))
        {
            // Run all routes
            foreach($tabRoute as $objRoute)
            {
                // Get info
                $strKey = $objRoute->getStrKey();
                $strSrc = $objRoute->getStrHandleSource();
                $strDesc = ToolBoxInfoRoute::getStrRouteDesc($objRoute);

                // Build info array if source valid
                if(!is_null($strSrc))
                {
                    // Init source if required
                    if(!array_key_exists($strSrc, $tabInfoCmd))
                    {
                        $tabInfoCmd[$strSrc] = array();
                    }

                    $tabInfoCmd[$strSrc][$strKey] = (is_null($strDesc) ? '' : $strDesc);
                }
            }
        }

        // Check array of commands info is valid and build summary
        if(count($tabInfoCmd) > 0)
        {
            // Run all routes
            $tabData = array();
            ksort($tabInfoCmd);
            foreach($tabInfoCmd as $strSrc => $tabRouteInfo)
            {
                // Build data in case: only 1 route associated to command
                $strInfoCmd = null;
                if(count($tabRouteInfo) == 1)
                {
                    $tabData[] = array(
                        $strSrc,
                        array_values($tabRouteInfo)[0]
                    );
                }
                // Build data in case else: multiple routes associated to command
                else
                {
                    ksort($tabRouteInfo);
                    foreach($tabRouteInfo as $strRouteKey => $strRouteDesc)
                    {
                        $tabData[] = array(
                            sprintf($strFormatSrcWithRoute, $strSrc, $strRouteKey),
                            array_values($tabRouteInfo)[0]
                        );
                    }
                }
            }

            // Build result
            $result = static::getStrTable($tabData);
        }

        // Return result
        return $result;
    }





	// Methods getters response
	// ******************************************************************************

	/**
     * Get information command response.
     *
	 * @param CommandFrontController $objFrontController
	 * @param DefaultResponse $objResponse = null
     * @return null|DefaultResponse
     */
    public static function getObjCommandResponse(CommandFrontController $objFrontController, DefaultResponse $objResponse = null)
    {
		// Init var
		$result = $objResponse;
        $objRequest = $objFrontController->getObjActiveRequest();
        $objRouter = $objFrontController->getObjRouter();

        // Check active request and router found
        if((!is_null($objRequest)) && (!is_null($objRouter)))
        {
            // Init var
            $strRoute = ToolBoxCommandRoute::getStrRouteFromRequestSrc($objRequest->getStrRouteSrc());
            $objRouteCollection = $objRouter->getObjRouteCollection();

            // Check string route and route collection found
            if((!is_null($strRoute)) && (!is_null($objRouteCollection)))
            {
                // Get string command information
                $strInfoCmd = static::getStrCommandContent($objRouteCollection, $strRoute);

                // Get object response if content found
                if(!is_null($strInfoCmd))
                {
                    // Init result, if required
                    if(is_null($result))
                    {
                        $result = new DefaultResponse();
                    }

                    // Register content
                    $result->setContent($strInfoCmd);
                }
            }
        }

		// Return result
		return $result;
    }



    /**
     * Get information summary response.
     *
     * @param CommandFrontController $objFrontController
     * @param DefaultResponse $objResponse = null
     * @return null|DefaultResponse
     */
    public static function getObjSummaryResponse(CommandFrontController $objFrontController, DefaultResponse $objResponse = null)
    {
        // Init var
        $result = $objResponse;
        $objRouter = $objFrontController->getObjRouter();

        // Check router found
        if(!is_null($objRouter))
        {
            // Init var
            $objRouteCollection = $objRouter->getObjRouteCollection();

            // Check route collection found
            if(!is_null($objRouteCollection))
            {
                // Get string summary information
                $strInfoSum = static::getStrSummaryContent($objRouteCollection);

                // Get object response if content found
                if(!is_null($strInfoSum))
                {
                    // Init result, if required
                    if(is_null($result))
                    {
                        $result = new DefaultResponse();
                    }

                    // Register content
                    $result->setContent($strInfoSum . PHP_EOL);
                }
            }
        }

        // Return result
        return $result;
    }
	
	
	
}