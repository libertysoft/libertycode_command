<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\command\request_flow\front\library;



class ConstCommandFrontController
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************
	
	// Exception message constants
	const EXCEPT_MSG_ACTIVE_REQUEST_INVALID_FORMAT = 'Following active request "%1$s" invalid! It must be a command request object.';
	const EXCEPT_MSG_DEFAULT_RESPONSE_INVALID_FORMAT = 'Following default response "%1$s" invalid! It must be a default response object.';
}