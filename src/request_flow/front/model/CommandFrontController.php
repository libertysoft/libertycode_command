<?php
/**
 * Description :
 * This class allows to define command front controller class.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\command\request_flow\front\model;

use liberty_code\request_flow\front\model\DefaultFrontController;

use liberty_code\route\route\api\RouteInterface;
use liberty_code\route\router\api\RouterInterface;
use liberty_code\request_flow\response\model\DefaultResponse;
use liberty_code\request_flow\front\library\ConstFrontController;
use liberty_code\command\config\model\DefaultConfig;
use liberty_code\command\route\library\ToolBoxCommandRoute;
use liberty_code\command\request_flow\request\model\CommandRequest;
use liberty_code\command\request_flow\front\exception\ActiveRequestInvalidFormatException;
use liberty_code\command\request_flow\front\exception\DefaultResponseInvalidFormatException;



class CommandFrontController extends DefaultFrontController
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor / Others
    // ******************************************************************************
	
	/**
     * @inheritdoc
     * @param RouterInterface $objRouter = null
     * @param CommandRequest $objActiveRequest = null
	 * @param DefaultResponse $objDefaultResponse = null
	 * @param boolean $boolDefaultResponseUse = null
	 * @param string|integer $optSelectReponse = null
     */
    public function __construct(
        RouterInterface $objRouter = null,
        CommandRequest $objActiveRequest = null,
        DefaultResponse $objDefaultResponse = null,
        $boolDefaultResponseUse = null,
        $optSelectReponse = null
    )
    {
        // Call parent constructor
        parent::__construct(
            $objRouter,
            $objActiveRequest,
            $objDefaultResponse,
            $boolDefaultResponseUse,
            $optSelectReponse
        );
    }





	// Methods validation
	// ******************************************************************************

	/**
	 * @inheritdoc
	 */
	public function beanCheckValidValue($key, $value, &$error = null)
	{
		// Init var
		$result = true;

		// Validation
		try
		{
			switch($key)
			{
				case ConstFrontController::DATA_KEY_ACTIVE_REQUEST:
					ActiveRequestInvalidFormatException::setCheck($value);
					break;

				case ConstFrontController::DATA_KEY_DEFAULT_RESPONSE:
					DefaultResponseInvalidFormatException::setCheck($value);
					break;

				default:
					$result = parent::beanCheckValidValue($key, $value, $error);
					break;
			}
		}
		catch(\Exception $e)
		{
			$result = false;
			$error = $e;
		}

		// Return result
		return $result;
	}





    // Methods getters
    // ******************************************************************************

    /**
     * Get specified argument from active request, formatted with specified route (by default active route).
     * Default value returned if argument not found, or argument not have valid type value.
     *
     * @param string $strName
     * @param mixed $default = null
     * @param RouteInterface $objRoute = null
     * @return mixed
     */
    public function getArgValue($strName, $default = null, RouteInterface $objRoute = null)
    {
        // Init var
        if(is_null($objRoute))
        {
            $objRoute = $this->getObjActiveRoute();
        }

        // Return result
        return ToolBoxCommandRoute::getArgValue($strName, $this->getObjActiveRequest(), $objRoute, $default);
    }



    /**
     * Get specified option from active request, formatted with specified route (by default active route).
     * Default value returned if option not found, or option not have valid type value.
     *
     * @param string $strName
     * @param mixed $default = false
     * @param RouteInterface $objRoute = null
     * @return mixed
     */
    public function getOptValue($strName, $default = false, RouteInterface $objRoute = null)
    {
        // Init var
        if(is_null($objRoute))
        {
            $objRoute = $this->getObjActiveRoute();
        }

        // Return result
        return ToolBoxCommandRoute::getOptValue($strName, $this->getObjActiveRequest(), $objRoute, $default);
    }
	




	// Methods execute
	// ******************************************************************************
	
	/**
	 * @inheritdoc
	 * @return null|DefaultResponse
	 */
	protected function getObjResponseFromExec($objResponse)
	{
		// Init var
		$result = null;
		$objDefaultResponse = $this->getObjDefaultResponse();
		$boolDefaultResponse = $this->checkDefaultResponseUse();
		
		// Get specified response if valid
		if($objResponse instanceof DefaultResponse)
		{
			$result = $objResponse;
		}
		// Get default response if required and valid
		else if($boolDefaultResponse && (!is_null($objDefaultResponse)))
		{
			$result = $objDefaultResponse;
		}

		// Return result
        return $result;
	}





    // Methods statics
    // ******************************************************************************

    /**
     * Get configuration.
     *
     * @return DefaultConfig
     */
    public static function getObjConfigCommand()
    {
        // Return result
        return DefaultConfig::instanceGetDefault();
    }



}