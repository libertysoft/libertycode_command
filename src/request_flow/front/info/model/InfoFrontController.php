<?php
/**
 * Description :
 * This class allows to define information command front controller class.
 * It allows to show information about a specific command, or the summary of commands.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\command\request_flow\front\info\model;

use liberty_code\command\request_flow\front\model\CommandFrontController;

use liberty_code\route\router\api\RouterInterface;
use liberty_code\request_flow\response\model\DefaultResponse;
use liberty_code\command\route\library\ToolBoxCommandRoute;
use liberty_code\command\request_flow\request\model\CommandRequest;
use liberty_code\command\request_flow\front\info\library\ConstInfoFrontController;
use liberty_code\command\request_flow\front\info\exception\InfoCmdOptInvalidFormatException;
use liberty_code\command\request_flow\front\info\exception\InfoSumOptInvalidFormatException;
use liberty_code\command\request_flow\front\info\exception\InfoCmdResponseInvalidFormatException;
use liberty_code\command\request_flow\front\info\exception\InfoSumResponseInvalidFormatException;
use liberty_code\command\request_flow\front\info\exception\InfoOptNameInvalidFormatException;



/**
 * @method null|DefaultResponse getObjInfoCommandResponse() Get response object for information command.
 * @method null|DefaultResponse getObjInfoSummaryResponse() Get response object for information summary.
 * @method array getTabInfoOptionName() Get index array of information option names.
 * @method void setBoolInfoCommandOpt(boolean $boolInfoCommandOpt) Set information command option.
 * @method void setBoolInfoSummaryOpt(boolean $boolInfoSummaryOpt) Set information summary option.
 * @method void setObjInfoCommandResponse(DefaultResponse $objResponse) Set response object for information command.
 * @method void setObjInfoSummaryResponse(DefaultResponse $objResponse) Set response object for information summary.
 * @method void setTabInfoOptionName(array $tabInfoOptionName) Set index array of information option names.
 */
class InfoFrontController extends CommandFrontController
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor / Others
    // ******************************************************************************
	
	/**
     * @inheritdoc
     * @param RouterInterface $objRouter = null
     * @param CommandRequest $objActiveRequest = null
	 * @param DefaultResponse $objDefaultResponse = null
	 * @param boolean $boolDefaultResponseUse = null
	 * @param string|integer $optSelectReponse = null
     * @param boolean $boolInfoCommandOpt = null
     * @param boolean $boolInfoSummaryOpt = null
     * @param DefaultResponse $objInfoCommandResponse = null
     * @param DefaultResponse $objInfoSummaryResponse = null
     * @param array $tabInfoOptionName = null
     */
    public function __construct(
        RouterInterface $objRouter = null,
        CommandRequest $objActiveRequest = null,
        DefaultResponse $objDefaultResponse = null,
        $boolDefaultResponseUse = null,
        $optSelectReponse = null,
        $boolInfoCommandOpt = null,
        $boolInfoSummaryOpt = null,
        DefaultResponse $objInfoCommandResponse = null,
        DefaultResponse $objInfoSummaryResponse = null,
        array $tabInfoOptionName = null
    )
    {
        // Init option info command
        if(!is_null($boolInfoCommandOpt))
        {
            $this->setBoolInfoCommandOpt($boolInfoCommandOpt);
        }

        // Init option info summary
        if(!is_null($boolInfoSummaryOpt))
        {
            $this->setBoolInfoSummaryOpt($boolInfoSummaryOpt);
        }

        // Init response info command
        if(!is_null($objInfoCommandResponse))
        {
            $this->setObjInfoCommandResponse($objInfoCommandResponse);
        }

        // Init response info summary
        if(!is_null($objInfoSummaryResponse))
        {
            $this->setObjInfoSummaryResponse($objInfoSummaryResponse);
        }

        // Init array of option info names
        if(!is_null($tabInfoOptionName))
        {
            $this->setTabInfoOptionName($tabInfoOptionName);
        }

        // Call parent constructor
        parent::__construct($objRouter, $objActiveRequest, $objDefaultResponse, $boolDefaultResponseUse, $optSelectReponse);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * Overwrite hydrateDefault().
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstInfoFrontController::DATA_KEY_INFO_COMMAND_OPT))
        {
            $this->beanAdd(ConstInfoFrontController::DATA_KEY_INFO_COMMAND_OPT, true);
        }

        if(!$this->beanExists(ConstInfoFrontController::DATA_KEY_INFO_SUMMARY_OPT))
        {
            $this->beanAdd(ConstInfoFrontController::DATA_KEY_INFO_SUMMARY_OPT, true);
        }

        if(!$this->beanExists(ConstInfoFrontController::DATA_KEY_INFO_COMMAND_RESPONSE))
        {
            $this->beanAdd(ConstInfoFrontController::DATA_KEY_INFO_COMMAND_RESPONSE, null);
        }

        if(!$this->beanExists(ConstInfoFrontController::DATA_KEY_INFO_SUMMARY_RESPONSE))
        {
            $this->beanAdd(ConstInfoFrontController::DATA_KEY_INFO_SUMMARY_RESPONSE, null);
        }

        if(!$this->beanExists(ConstInfoFrontController::DATA_KEY_INFO_OPTION_NAME))
        {
            $this->beanAdd(ConstInfoFrontController::DATA_KEY_INFO_OPTION_NAME, ConstInfoFrontController::DATA_DEFAULT_VALUE_INFO_OPTION_NAME);
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





	// Methods validation
	// ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstInfoFrontController::DATA_KEY_INFO_COMMAND_OPT,
            ConstInfoFrontController::DATA_KEY_INFO_SUMMARY_OPT,
            ConstInfoFrontController::DATA_KEY_INFO_COMMAND_RESPONSE,
            ConstInfoFrontController::DATA_KEY_INFO_SUMMARY_RESPONSE,
            ConstInfoFrontController::DATA_KEY_INFO_OPTION_NAME
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



	/**
	 * @inheritdoc
	 */
	public function beanCheckValidValue($key, $value, &$error = null)
	{
		// Init var
		$result = true;

		// Validation
		try
		{
			switch($key)
			{
				case ConstInfoFrontController::DATA_KEY_INFO_COMMAND_OPT:
                    InfoCmdOptInvalidFormatException::setCheck($value);
					break;

				case ConstInfoFrontController::DATA_KEY_INFO_SUMMARY_OPT:
                    InfoSumOptInvalidFormatException::setCheck($value);
					break;

                case ConstInfoFrontController::DATA_KEY_INFO_COMMAND_RESPONSE:
                    InfoCmdResponseInvalidFormatException::setCheck($value);
                    break;

                case ConstInfoFrontController::DATA_KEY_INFO_SUMMARY_RESPONSE:
                    InfoSumResponseInvalidFormatException::setCheck($value);
                    break;

                case ConstInfoFrontController::DATA_KEY_INFO_OPTION_NAME:
                    InfoOptNameInvalidFormatException::setCheck($value);
                    break;

				default:
					$result = parent::beanCheckValidValue($key, $value, $error);
					break;
			}
		}
		catch(\Exception $e)
		{
			$result = false;
			$error = $e;
		}

		// Return result
		return $result;
	}





    // Methods check
    // ******************************************************************************

    /**
     * Check if information command option is selected.
     *
     * @return boolean
     */
    public function checkInfoCommandOpt()
    {
        // Return result
        return $this->beanGet(ConstInfoFrontController::DATA_KEY_INFO_COMMAND_OPT);
    }



    /**
     * Check if information summary option is selected.
     *
     * @return boolean
     */
    public function checkInfoSummaryOpt()
    {
        // Return result
        return $this->beanGet(ConstInfoFrontController::DATA_KEY_INFO_SUMMARY_OPT);
    }



    /**
     * Check if request requires information command.
     *
     * @return boolean
     */
    public function checkRequestInfoCommand()
    {
        // Init var
        $result = false;
        $objRequest = $this->getObjActiveRequest();

        // Check active request provided
        if(!is_null($objRequest))
        {
            // Get info
            $tabOptName = array_values($this->getTabInfoOptionName());
            /** @var CommandRequest $objRequest */
            $tabRequestOptKey = array_keys($objRequest->getOpt());

            // Check info command required
            $result =
                (!$this->checkRequestInfoSummary()) && // Not in summary required case (route provided)
                (count($objRequest->getArg()) == 0) && // Check no argument provided
                (count($tabRequestOptKey) == 1) && // Check only 1 option provided
                in_array($tabRequestOptKey[0], $tabOptName) && // Check option provided is info option name
                is_null($objRequest->getOpt($tabRequestOptKey[0])); // Check no value provided to option
        }

        // Return result
        return $result;
    }



    /**
     * Check if request requires information summary.
     *
     * @return boolean
     */
    public function checkRequestInfoSummary()
    {
        // Init var
        $result = false;
        $objRequest = $this->getObjActiveRequest();

        // Check active request provided
        if(!is_null($objRequest))
        {
            // Get route
            $strRoute = ToolBoxCommandRoute::getStrRouteFromRequestSrc($objRequest->getStrRouteSrc());

            // Info summary required if no route provided
            $result =
                is_null($strRoute) ||
                (is_string($strRoute) && (trim($strRoute) == ''));
        }

        // Return result
        return $result;
    }
	




	// Methods execute
	// ******************************************************************************

    /**
     * @inheritdoc
     */
    public function execute()
    {
        // Init var
        $result = null;

        // Get information summary response, if required
        if($this->checkInfoSummaryOpt() && $this->checkRequestInfoSummary())
        {
            $result = $this->getObjInfoSummaryResponse();
        }
        // Get information command response, if required
        else if($this->checkInfoCommandOpt() && $this->checkRequestInfoCommand())
        {
            $result = $this->getObjInfoCommandResponse();
        }

        // Call parent method, if summary or command information not required, or not found.
        if(is_null($result))
        {
            $result = parent::execute();
        }

        // Return result
        return $result;
    }
	


}