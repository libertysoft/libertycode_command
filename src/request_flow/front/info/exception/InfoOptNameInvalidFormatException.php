<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\command\request_flow\front\info\exception;

use liberty_code\command\request_flow\front\info\library\ConstInfoFrontController;



class InfoOptNameInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $name
     */
	public function __construct($name)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
		(
            ConstInfoFrontController::EXCEPT_MSG_INFO_OPTION_NAME_INVALID_FORMAT,
            mb_strimwidth(strval($name), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if name has valid format.
	 * 
     * @param mixed $name
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($name)
    {
		// Init var
		$result = is_array($name); // Check is valid array
		
		// Run all names
        $name = array_values($name);
		for($intCpt = 0; $result && ($intCpt < count($name)); $intCpt++)
		{
			// Check name is valid string
			$result = (is_string($name[$intCpt]) && (trim($name[$intCpt]) !== ''));
		}
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($name) ? serialize($name) : $name));
		}
		
		// Return result
		return $result;
    }
	
	
	
}