<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\command\request_flow\front\info\exception;

use liberty_code\command\request_flow\front\info\library\ConstInfoFrontController;



class InfoSumOptInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $option
     */
	public function __construct($option)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstInfoFrontController::EXCEPT_MSG_INFO_SUMMARY_OPT_INVALID_FORMAT,
            mb_strimwidth(strval($option), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified option has valid format
	 * 
     * @param mixed $option
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($option)
    {
		// Init var
		$result = is_bool($option); // Check is boolean
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($option);
		}
		
		// Return result
		return $result;
    }
	
	
	
}