<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\command\request_flow\front\info\library;



class ConstInfoFrontController
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_INFO_COMMAND_OPT = 'boolInfoCommandOpt';
    const DATA_KEY_INFO_SUMMARY_OPT = 'boolInfoSummaryOpt';
    const DATA_KEY_INFO_COMMAND_RESPONSE = 'objInfoCommandResponse';
    const DATA_KEY_INFO_SUMMARY_RESPONSE = 'objInfoSummaryResponse';
    const DATA_KEY_INFO_OPTION_NAME = 'tabInfoOptionName';

    const DATA_DEFAULT_VALUE_INFO_OPTION_NAME = array(
        'help', // Help long option
        'h' // Help short option
    );



    // Exception message constants
    const EXCEPT_MSG_INFO_COMMAND_OPT_INVALID_FORMAT = 'Following information command option "%1$s" invalid! The option must be a boolean.';
    const EXCEPT_MSG_INFO_SUMMARY_OPT_INVALID_FORMAT = 'Following information summary option "%1$s" invalid! The option must be a boolean.';
    const EXCEPT_MSG_INFO_COMMAND_RESPONSE_INVALID_FORMAT = 'Following information command response "%1$s" invalid! It must be a default response object.';
    const EXCEPT_MSG_INFO_SUMMARY_RESPONSE_INVALID_FORMAT = 'Following information summary response "%1$s" invalid! It must be a default response object.';
    const EXCEPT_MSG_INFO_OPTION_NAME_INVALID_FORMAT = 'Following information option names "%1$s" invalid! The option names must be an array of string, not empty.';
}