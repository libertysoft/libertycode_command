<?php

namespace liberty_code\command\request_flow\front\test;

use liberty_code\request_flow\response\library\ToolBoxResponse;
use liberty_code\command\request_flow\request\model\CommandRequest;
use liberty_code\command\request_flow\front\model\CommandFrontController;
use liberty_code\command\request_flow\front\info\model\InfoFrontController;



class ControllerTest1
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods action
    // ******************************************************************************

	public function actionCmd($strAdd = '', $strAdd2 = '')
    {
		// Init var
		/** @var CommandFrontController $objFrontController */
		$objFrontController = (
		    CommandFrontController::instanceCheckIndexExists(0) ?
            CommandFrontController::instanceGet(0) :
            InfoFrontController::instanceGet(0)
        );

		/** @var CommandRequest $objActiveRequest */
		$objActiveRequest = CommandRequest::instanceGetDefault();
		
		// Init active route(s)
		$tabStrRoute = array();
		$tabRoute = $objFrontController->getTabActiveRoute();
		foreach($tabRoute as $objRoute)
		{
			$tabStrRoute[] = $objRoute->getStrKey();
		}
		
		// Get data
		$tabData = array(
			'argument' => [
				'request' => [
					'add_1' => $strAdd,
					'add_2' => $strAdd2,
				],
				'argument' => $objActiveRequest->getArg(),
				'option' => $objActiveRequest->getOpt(),
			],
			'route' => $tabStrRoute
		);
		
		// Get response
		$objResponse = ToolBoxResponse::getObjJsonResponse($tabData);
		
        // Return result
        return $objResponse;
    }
	
	
	
	public function actionCmd2($strAdd = '')
    {
		// Init var
		/** @var CommandFrontController $objFrontController */
		/*
		$objFrontController = $objFrontController = (
		    CommandFrontController::instanceCheckIndexExists(0) ?
            CommandFrontController::instanceGet(0) :
            InfoFrontController::instanceGet(0)
        );
		//*/

		// return ToolBoxResponse::getObjJsonResponse(array());
    }
	
	
	
}