<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Load test
require_once($strRootAppPath . '/src/request_flow/front/test/ControllerTest1.php');
require_once($strRootAppPath . '/src/request_flow/front/test/ControllerTest2.php');

// Use
use liberty_code\register\register\memory\model\MemoryRegister;
use liberty_code\di\dependency\preference\model\Preference;
use liberty_code\di\dependency\model\DefaultDependencyCollection;
use liberty_code\di\provider\model\DefaultProvider;
use liberty_code\call\call\file\library\ConstFileCall;
use liberty_code\call\call\di\func\library\ConstFunctionCall;
use liberty_code\call\call\factory\file\model\FileCallFactory;
use liberty_code\call\call\factory\di\model\DiCallFactory;
use liberty_code\route\route\model\DefaultRouteCollection;
use liberty_code\route\build\model\DefaultBuilder;
use liberty_code\route\router\model\DefaultRouter;
use liberty_code\command\route\factory\model\CommandRouteFactory;



// Init DI
$objRegister = new MemoryRegister();
$objDepCollection = new DefaultDependencyCollection($objRegister);
$objProvider = new DefaultProvider($objDepCollection);

$objPref = new Preference(array(
    'source' => 'liberty_code\\di\\provider\\api\\ProviderInterface',
    'set' =>  ['type' => 'instance', 'value' => $objProvider],
    'option' => [
        'shared' => true
    ]
));
$objProvider->getObjDependencyCollection()->setDependency($objPref);



// Init call factory
$tabConfig = array(
    ConstFileCall::TAB_CONFIG_KEY_FILE_PATH_FORMAT_PATTERN => $strRootAppPath . '/%1$s',
    ConstFileCall::TAB_CONFIG_KEY_FILE_PATH_FORMAT_REQUIRE => true,
    ConstFunctionCall::TAB_CONFIG_KEY_FILE_PATH_FORMAT_PATTERN => $strRootAppPath . '/%1$s',
    ConstFunctionCall::TAB_CONFIG_KEY_FILE_PATH_FORMAT_REQUIRE => true
);

$objCallFactory = new FileCallFactory(
    $tabConfig
);

$objCallFactory = new DiCallFactory(
    $objProvider,
    $tabConfig,
    null,
    null,
    $objCallFactory
);



// Init router
$objRouteCollection = new DefaultRouteCollection($objCallFactory);
$objRouteCollection->setMultiMatch(false);
//$objRouteCollection->setMultiMatch(true);
$objRouter = new DefaultRouter($objRouteCollection);



// Hydrate route collection
$objRouteFactory = new CommandRouteFactory();
$objRouteBuilder = new DefaultBuilder($objRouteFactory);

$tabDataSrc = array(
    'route_1' => [
        'source' => 'hello:{strAdd}:{strAdd2}:test',
        'description' => 'Description command: Route 1',
        'call' => [
            'class_path_pattern' => 'liberty_code\\command\\request_flow\\front\\test\\ControllerTest1:actionCmd'
        ]
    ],
    'route_2' => [
        'source' => 'hello:{strAdd}:test2',
        'description' => 'Description command: Route 2',
        'source_argument' => [
            [
                'type' => 'argument',
                'name' => 'arg_test_1',
                'description' => 'Description argument: arg_test_1',
                'type_value' => 'numeric',
                'enum_value' => [1, 1.2, 1.5, 1.7]
            ],
            [
                'type' => 'argument',
                'name' => 'arg_test_2',
                'description' => 'Description argument: arg_test_2',
                'type_value' => 'boolean',
                'enum_value' => ['TRUE']
            ],
            [
                'type' => 'option',
                'name' => ['opt_1', 'a'],
                'description' => 'Description option: a',
                'required' => false,
                'required_value' => false,
                'type_value' => 'string',
                'enum_value' => ['test-1', 'Test_3', 'Test 7']
            ]
        ],
        'call' => [
            'class_path_pattern' => 'liberty_code\\command\\request_flow\\front\\test\\ControllerTest2:actionCmd'
        ]
    ],
    'route_3' => [
        'source' => 'hello:{strAdd}:test3',
        'source_argument' => [
            [
                'type' => 'option',
                'name' => ['test2', 't'],
                'description' => 'Description option: test2 (t)',
                'required' => true,
                'required_value' => true,
                'type_value' => 'integer'
            ],
            [
                'type' => 'option',
                'name' => ['a'],
                'description' => 'Description option: a',
                'required' => false,
                'required_value' => false,
                'type_value' => 'boolean'
            ]
        ],
        'call' => [
            'class_path_pattern' => 'liberty_code\\command\\request_flow\\front\\test\\ControllerTest2:actionCmd2'
        ]
    ],
    //*
    'route_pattern_1' => [
        'type' => 'command_pattern',
        'source' => '.*:([^:]+):test\d$',
        'call' => [
            'class_path_pattern' => 'liberty_code\\command\\request_flow\\front\\test\\ControllerTest1:actionCmd2'
        ]
    ]
    //*/
);

$objRouteBuilder->setTabDataSrc($tabDataSrc);
$objRouteBuilder->hydrateRouteCollection($objRouteCollection);


