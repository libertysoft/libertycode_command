<?php

namespace liberty_code\command\request_flow\front\test;

use liberty_code\request_flow\response\library\ToolBoxResponse;
use liberty_code\command\request_flow\request\model\CommandRequest;
use liberty_code\command\request_flow\front\model\CommandFrontController;
use liberty_code\command\request_flow\front\info\model\InfoFrontController;



class ControllerTest2
{
    // ******************************************************************************
    // Methods
    // ******************************************************************************
	
    // Methods action
    // ******************************************************************************

	public function actionCmd($strAdd = '')
    {
		// Init var
		/** @var CommandFrontController $objFrontController */
		$objFrontController = $objFrontController = (
            CommandFrontController::instanceCheckIndexExists(0) ?
            CommandFrontController::instanceGet(0) :
            InfoFrontController::instanceGet(0)
        );

		/** @var CommandRequest $objActiveRequest */
		$objActiveRequest = CommandRequest::instanceGetDefault();
		
		// Init active route(s)
		$tabStrRoute = array();
		$tabRoute = $objFrontController->getTabActiveRoute();
		foreach($tabRoute as $objRoute)
		{
			$tabStrRoute[$objRoute->getStrKey()] = get_class($objRoute);
		}
		
		// Get data
		$tabData = array(
			'root' => [
				'argument' => [
					'request' => [
						'add' => $strAdd
					],
					'argument' => $objActiveRequest->getArg(),
					'option' => $objActiveRequest->getOpt(),
				],
				'route' => $tabStrRoute,
                'arg_test_1' => $objFrontController->getArgValue('arg_test_1', 'not found'),
                'arg_test_2' => $objFrontController->getArgValue('arg_test_2', 'not found'),
                'arg_test_3' => $objFrontController->getArgValue('arg_test_3', 'not found'),
                'opt_test_1' => $objFrontController->getOptValue('opt_1', 'not found'),
                'opt_test_1_1' => $objFrontController->getOptValue('a', 'not found'),
                'opt_test_2' => $objFrontController->getOptValue('opt_2', 'not found'),
                'opt_test_2_1' => $objFrontController->getOptValue('b', 'not found')
			]
		);
		
		// Get response
		$objResponse = ToolBoxResponse::getObjXmlResponse($tabData);

        // Return result
        return $objResponse;
    }
	
	
	
	public function actionCmd2($strAdd = '')
    {
        // Init var
		/** @var CommandFrontController $objFrontController */
		$objFrontController = (
            CommandFrontController::instanceCheckIndexExists(0) ?
            CommandFrontController::instanceGet(0) :
            InfoFrontController::instanceGet(0)
        );
		
		// Get forwarded response
		$objResponse = $objFrontController->executeRoute('route_2', array('strAdd' => $strAdd . '-forward'));
		
        // Return result
        return $objResponse;
    }
	
	
	
}