<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\command\request_flow\request\exception;

use liberty_code\command\request_flow\request\library\ConstCommandRequest;



class RouteNotFoundException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     */
	public function __construct() 
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = ConstCommandRequest::EXCEPT_MSG_ROUTE_NOT_FOUND;
	}
	
	
	
}