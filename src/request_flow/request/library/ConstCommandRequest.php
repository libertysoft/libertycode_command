<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\command\request_flow\request\library;



class ConstCommandRequest
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Route source configuration
    const CONF_ROUTE_SOURCE_KEY_ROUTE = 'route';
    const CONF_ROUTE_SOURCE_KEY_ARGUMENT = 'argument';
    const CONF_ROUTE_SOURCE_KEY_OPTION = 'option';



	// Exception message constants
	const EXCEPT_MSG_ROUTE_NOT_FOUND = 'Route not found in command arguments!';
} 