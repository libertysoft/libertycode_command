<?php
/**
 * Description :
 * This class allows to define command request class.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\command\request_flow\request\model;

use liberty_code\request_flow\request\model\DefaultRequest;

use liberty_code\library\table\library\ToolBoxTable;
use liberty_code\command\config\model\DefaultConfig;
use liberty_code\command\request_flow\request\library\ConstCommandRequest;
use liberty_code\command\request_flow\request\exception\RouteNotFoundException;



class CommandRequest extends DefaultRequest
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	/** @var null|array */
	protected $tabArg;
	
	/** @var null|array */
	protected $tabOpt;
	
	/** @var null|string */
	protected $strRouteSrc;

    /**
     * Specify if route source is composed (include arguments and options)
     * @var null|boolean
     */
    protected $boolRouteSrcComposed;
	


	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * Constructor
     *
     * @param boolean $boolRouteSrcComposed = true
     */
    public function __construct($boolRouteSrcComposed = true)
    {
		// Call parent constructor
		parent::__construct();

        // Init var
		$this->tabArg = null;
		$this->tabOpt = null;
		$this->strRouteSrc = null;
        $this->boolRouteSrcComposed = null;

		// Init arguments, options
		static::setTabRequestArgOpt($this->tabArg, $this->tabOpt);

		// Init route source composed option
        $this->setRouteSrcComposed($boolRouteSrcComposed);
    }
	
	
	
	
	
	 // Methods check
	// ******************************************************************************

    /**
     * Check if route source is composed.
     *
     * @return boolean
     */
    public function checkRouteSrcComposed()
    {
        // Return result
        return $this->boolRouteSrcComposed;
    }



	/**
     * Check if specified argument exists.
     *
	 * @param integer $intIndex
     * @return boolean
     */
    public function checkArgExists($intIndex)
	{
		// Return result
		return (!is_null($this->getArg($intIndex, null)));
	}
	
	
	
	/**
     * Check if specified option exists.
     *
	 * @param string $strKey
     * @return boolean
     */
    public function checkOptExists($strKey)
	{
		// Return result
		return ($this->getOpt($strKey, false) !== false);
	}
	
	
	
	
	
	// Methods getters
    // ******************************************************************************

	/**
     * Get argument(s).
	 * If specified key provided: get specified argument.
	 * Else: get index array of arguments.
     *
	 * @param integer $intIndex = null
	 * @param mixed $default = null
     * @return array|string|mixed
     */
    public function getArg($intIndex = null, $default = null)
	{
		// Return result
		return ToolBoxTable::getItem($this->tabArg, $intIndex, $default);
	}
	
	
	
	/**
     * Get option(s).
	 * If specified key provided: get specified option.
	 * Else: get associative array of options.
     *
	 * @param string $strKey = null
	 * @param mixed $default = false
     * @return array|string|mixed
     */
    public function getOpt($strKey = null, $default = false)
	{
		// Return result
		return ToolBoxTable::getItem($this->tabOpt, $strKey, $default);
	}
	
	
	
	/**
	 * @inheritdoc
	 * @throws RouteNotFoundException
	 */
	public function getStrRouteSrc()
	{
		// Init route source if required
		if(is_null($this->strRouteSrc))
		{
		    // Build route source composed, if required
		    if($this->checkRouteSrcComposed())
            {
                $this->strRouteSrc = json_encode(
                    array(
                        ConstCommandRequest::CONF_ROUTE_SOURCE_KEY_ROUTE => static::getStrRequestRouteSrc(),
                        ConstCommandRequest::CONF_ROUTE_SOURCE_KEY_ARGUMENT => $this->tabArg,
                        ConstCommandRequest::CONF_ROUTE_SOURCE_KEY_OPTION => $this->tabOpt
                    )
                );
            }
            // Else: build route source
            else
            {
                $this->strRouteSrc = static::getStrRequestRouteSrc();
            }

			// Throw exception if route not found
			if(is_null($this->strRouteSrc))
			{
				throw new RouteNotFoundException();
			}
		}
		
		// Return result
		return $this->strRouteSrc;
	}





    // Methods setters
    // ******************************************************************************

    /**
     * Set route source composed option.
     *
     * @param boolean $boolRouteSrcComposed = true
     */
    public function setRouteSrcComposed($boolRouteSrcComposed = true)
    {
        // Init argument, if required
        if(!is_bool($boolRouteSrcComposed))
        {
            $boolRouteSrcComposed = true;
        }

        // Set property
        $this->boolRouteSrcComposed = $boolRouteSrcComposed;

        // Reset route source
        $this->strRouteSrc = null;
    }




	
	// Methods static getters
    // ******************************************************************************

    /**
     * Get command line request flow configuration.
     *
     * @inheritdoc
     * @return DefaultConfig
     */
    public static function getObjConfig()
    {
        // Return result
        return DefaultConfig::instanceGetDefault();
    }



	/**
	 * Get index array of all arguments (file and route included).
	 * 
	 * @return array
	 */
	public static function getTabAllRequestArg()
	{
		// Init var
		$result = array();
		
		// Get array of arguments
		if(isset($_SERVER['argv']) && is_array($_SERVER['argv']))
		{
			$result = $_SERVER['argv'];
		}
		
		// Return result
		return $result;
	}
	
	
	
	/**
	 * Get index array of arguments.
	 * 
	 * @return array
	 */
	public static function getTabRequestArg()
	{
		// Init var
		$result = array();
		$tabArg = static::getTabAllRequestArg();
		
		// Run all arguments (Don't care first and second argument, considered as file and route)
		if(count($tabArg) >= 3)
		{
			for($intCpt = 2; $intCpt < count($tabArg); $intCpt++)
			{
				$result[] = $tabArg[$intCpt];
			}
		}
		
		// Return result
		return $result;
	}
	
	
	
	/**
	 * Get associative arguments and options array.
	 * 
	 * @param array &$tabArg: Arguments put on this array
	 * @param array &$tabOpt: Options put on this array
	 */
	public static function setTabRequestArgOpt(&$tabArg, &$tabOpt)
	{
		// Init var
		$tabArg = array();
		$tabOpt = array();
		$tabRequestArg = static::getTabRequestArg();
		$tabRegexpOpt = static::getObjConfig()->getTabCmdRequestOptRegexp();
		$tabRegexpOptValue = static::getObjConfig()->getTabCmdRequestOptValueRegexp();

		// Run all arguments
		foreach($tabRequestArg as $strArg)
		{
			$boolMatch = false;
			
			// Case option without value
			foreach($tabRegexpOpt as $strRegexp)
			{
				if(preg_match($strRegexp, $strArg, $tabMatch) == 1)
				{
					$strKey = $tabMatch[1];
					$tabOpt[$strKey] = null;
					$boolMatch = true;
					break;
				}
			}
			
			// Case option with value
			if(!$boolMatch)
			{
				foreach($tabRegexpOptValue as $strRegexp)
				{
					if(preg_match($strRegexp, $strArg, $tabMatch) == 1)
					{
						$strKey = $tabMatch[1];
						$strValue = $tabMatch[2];
						$tabOpt[$strKey] = $strValue;
						$boolMatch = true;
						break;
					}
				}
			}
			
			// Case else: considered as argument
			if(!$boolMatch)
			{
				$tabArg[] = $strArg;
			}
		}
	}
	
	
	
	/**
	 * Get string route source.
	 * 
	 * @return null|string
	 */
	public static function getStrRequestRouteSrc()
	{
		// Init var
		$result = null;
		$tabArg = static::getTabAllRequestArg();
		
		// Get route if found as first argument
		if(isset($tabArg[1]))
		{
			$result = $tabArg[1];
		}
		
		// Return result
		return $result;
	}
	
	
	
}