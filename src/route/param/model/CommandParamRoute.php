<?php
/**
 * Description :
 * This class allows to define command param route.
 * Command param route is param route, using command configuration source.
 *
 * Command param route uses the following specified configuration:
 * [
 *     Param route configuration,
 *
 *     description (optional): "string command description",
 *
 *     source_argument(optional): [
 *         List of arguments and options:
 *         - Argument item:
 *         [
 *             type(required): "argument",
 *             name(required: Each name must be alphanumeric, unique): "string argument name",
 *             description (optional): "string argument description",
 *             type_value (optional: got "string" if not found):
 *                 "string" / "numeric" / "integer" / "boolean" (type of value authorized),
 *             enum_value (optional): [
 *                 Authorized value 1 (same type like type_value),
 *                 ...,
 *                 Authorized value N
 *             ]
 *         ],
 *         ...,
 *         - Option item:
 *         [
 *             type(required): "option",
 *             name(required: At least 1 name. Each name must be alphanumeric, unique):
 *                 List option names in accordance to command REGEXP configuration.
*                  Following initial configuration:
 *                     ["string option long name", "string option short name", ...],
 *             description (optional): "string option description"
 *             required(required): true / false (option required),
 *             required_value(required): true / false (option value required),
 *             type_value (optional: got "string" if not found):
 *                 "string" / "numeric" / "integer" / "boolean" (type of value authorized),
 *             enum_value (optional): [
 *                 Authorized value 1 (same type like type_value),
 *                 ...,
 *                 Authorized value N
 *             ]
 *         ],
 *         ...,
 *     ]
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\command\route\param\model;

use liberty_code\route\route\param\model\ParamRoute;

use liberty_code\route\route\library\ConstRoute;
use liberty_code\command\route\library\ToolBoxCommandRoute;
use liberty_code\command\route\exception\ConfigInvalidFormatException;



class CommandParamRoute extends ParamRoute
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        // $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstRoute::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function checkMatches($strSrc)
    {
        // Init var
        $result = parent::checkMatches($strSrc); // Basic check match

        // Check source argument matches, if required
        if($result && ToolBoxCommandRoute::checkRequestSrcIsComposed($strSrc))
        {
            // Get info
            $tabRequestArg = ToolBoxCommandRoute::getTabArgFromRequestSrc($strSrc);
            $tabRequestOpt = ToolBoxCommandRoute::getTabOptFromRequestSrc($strSrc);
            $tabRouteArg = ToolBoxCommandRoute::getTabArgFromRouteConfig($this->getTabConfig());
            $tabRouteOpt = ToolBoxCommandRoute::getTabOptFromRouteConfig($this->getTabConfig());

            $result =
                // Check argument matches if configured
                (is_null($tabRouteArg) || ToolBoxCommandRoute::checkArgMatches($tabRequestArg, $tabRouteArg)) &&
                // Check option matches if configured
                (is_null($tabRouteOpt) || ToolBoxCommandRoute::checkOptMatches($tabRequestOpt, $tabRouteOpt));
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrMatchFormatSource($strSrc)
    {
        // Init var
        $result = $strSrc;

        // Get route in composed case, if required
        if(ToolBoxCommandRoute::checkRequestSrcIsComposed($strSrc))
        {
            $result = ToolBoxCommandRoute::getStrRouteFromRequestSrc($strSrc);
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getStrCallElmFormatSource($strSrc)
    {
        // Return result
        return $this->getStrMatchFormatSource($strSrc);
    }



    /**
     * @inheritdoc
     */
    protected function getStrCallArgFormatSource($strSrc)
    {
        // Return result
        return $this->getStrMatchFormatSource($strSrc);
    }



}