<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\command\route\library;



class ConstCommandRoute
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************
	
	// Configuration keys
    const TAB_CONFIG_KEY_DESCRIPTION = 'description';
	const TAB_CONFIG_KEY_SOURCE_ARGUMENT = 'source_argument';
    const TAB_CONFIG_KEY_SOURCE_ARGUMENT_TYPE = 'type';
    const TAB_CONFIG_KEY_SOURCE_ARGUMENT_NAME = 'name';
    const TAB_CONFIG_KEY_SOURCE_ARGUMENT_DESCRIPTION = 'description';
    const TAB_CONFIG_KEY_SOURCE_ARGUMENT_REQUIRED = 'required';
    const TAB_CONFIG_KEY_SOURCE_ARGUMENT_REQUIRED_VALUE = 'required_value';
    const TAB_CONFIG_KEY_SOURCE_ARGUMENT_TYPE_VALUE = 'type_value';
    const TAB_CONFIG_KEY_SOURCE_ARGUMENT_ENUM_VALUE = 'enum_value';

    const TAB_CONFIG_SOURCE_ARGUMENT_TYPE_ARGUMENT = 'argument';
    const TAB_CONFIG_SOURCE_ARGUMENT_TYPE_OPTION = 'option';

    const TAB_CONFIG_SOURCE_ARGUMENT_TYPE_VALUE_STRING = 'string';
    const TAB_CONFIG_SOURCE_ARGUMENT_TYPE_VALUE_NUMERIC = 'numeric';
    const TAB_CONFIG_SOURCE_ARGUMENT_TYPE_VALUE_INTEGER = 'integer';
    const TAB_CONFIG_SOURCE_ARGUMENT_TYPE_VALUE_BOOLEAN = 'boolean';



    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array, not empty and following the command line configuration standard.';
}