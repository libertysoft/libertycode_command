<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\command\route\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\route\route\api\RouteInterface;
use liberty_code\command\request_flow\response\info\library\ToolBoxInfoResponse;
use liberty_code\command\route\library\ConstCommandRoute;
use liberty_code\command\route\library\ToolBoxCommandRoute;
use liberty_code\command\route\exception\ConfigInvalidFormatException;



class ToolBoxInfoRoute extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods getters
    // ******************************************************************************

    /**
     * Get string description about specified route.
     *
     * @param RouteInterface $objRoute
     * @return null|string
     */
    public static function getStrRouteDesc(RouteInterface $objRoute)
    {
        // Set check route configuration
        ConfigInvalidFormatException::setCheck($objRoute->getTabConfig());

        // Init var
        $result = null;
        $tabConfig = $objRoute->getTabConfig();

        // Get description if found
        if(isset($tabConfig[ConstCommandRoute::TAB_CONFIG_KEY_DESCRIPTION]))
        {
            $result = $tabConfig[ConstCommandRoute::TAB_CONFIG_KEY_DESCRIPTION];
        }

        // Return result
        return $result;
    }



    /**
     * Get string source arguments information about specified route.
     *
     * @param RouteInterface $objRoute
     * @return null|string
     */
    public static function getStrRouteSrcArgInfo(RouteInterface $objRoute)
    {
        // Set check route configuration
        ConfigInvalidFormatException::setCheck($objRoute->getTabConfig());

        // Init var
        $result = null;
        $tabData = array();
        $strFormatArg = '- Argument "%1$s" (%2$s)';
        $strFormatOpt = '- Option %1$s (%2$s) %3$s %4$s';
        $strFormatOptNameFirst = '"%1$s"';
        $strFormatOptName = ', "%1$s"';
        $tabArg = ToolBoxCommandRoute::getTabArgFromRouteConfig($objRoute->getTabConfig());
        $tabOpt = ToolBoxCommandRoute::getTabOptFromRouteConfig($objRoute->getTabConfig());

        // Process arguments, if required
        if(!is_null($tabArg))
        {
            // Run all arguments
            foreach($tabArg as $tabArgInfo)
            {
                // Get info
                $strName = $tabArgInfo[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_NAME];
                $strType = (
                    isset($tabArgInfo[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_TYPE_VALUE]) ?
                    $tabArgInfo[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_TYPE_VALUE] :
                    ConstCommandRoute::TAB_CONFIG_SOURCE_ARGUMENT_TYPE_VALUE_STRING
                );
                $strEnum = (
                    (
                        isset($tabArgInfo[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_ENUM_VALUE]) &&
                        (count($tabArgInfo[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_ENUM_VALUE]) > 0)
                    ) ?
                        implode(', ', $tabArgInfo[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_ENUM_VALUE]) :
                        ''
                );
                $strType = (
                    (trim($strEnum) != '') ?
                        sprintf('%1$s: %2$s', $strType, $strEnum) :
                        $strType
                );
                $strDesc = (
                    isset($tabArgInfo[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_DESCRIPTION]) ?
                    $tabArgInfo[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_DESCRIPTION] :
                    ''
                );

                // Build argument data
                $tabData[] = array(
                    sprintf($strFormatArg, $strName, $strType),
                    $strDesc
                );
            }
        }

        // Process options, if required
        if(!is_null($tabOpt))
        {
            // Run all arguments
            foreach($tabOpt as $tabOptInfo)
            {
                // Get info
                $strType = (
                    isset($tabOptInfo[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_TYPE_VALUE]) ?
                    $tabOptInfo[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_TYPE_VALUE] :
                    ConstCommandRoute::TAB_CONFIG_SOURCE_ARGUMENT_TYPE_VALUE_STRING
                );
                $strEnum = (
                    (
                        isset($tabOptInfo[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_ENUM_VALUE]) &&
                        (count($tabOptInfo[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_ENUM_VALUE]) > 0)
                    ) ?
                        implode(', ', $tabOptInfo[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_ENUM_VALUE]) :
                        ''
                );
                $strType = (
                    (trim($strEnum) != '') ?
                        sprintf('%1$s: %2$s', $strType, $strEnum) :
                        $strType
                );
                $strRequire = (
                    isset($tabOptInfo[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_REQUIRED]) &&
                    (intval($tabOptInfo[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_REQUIRED]) != 0) ?
                    '[required]' :
                    ''
                );
                $strRequireValue = (
                    isset($tabOptInfo[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_REQUIRED_VALUE]) &&
                    (intval($tabOptInfo[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_REQUIRED_VALUE]) != 0) ?
                    '[value required]' :
                    ''
                );
                $strDesc = (
                    isset($tabOptInfo[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_DESCRIPTION]) ?
                    $tabOptInfo[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_DESCRIPTION] :
                    ''
                );

                // Get names
                $strName = '';
                foreach($tabOptInfo[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_NAME] as $strOptName)
                {
                    // In first case
                    if(trim($strName) == '')
                    {
                        $strName = sprintf($strFormatOptNameFirst, $strOptName);
                    }
                    // In standard case
                    else
                    {
                        $strName .= sprintf($strFormatOptName, $strOptName);
                    }
                }

                // Build argument data
                $tabData[] = array(
                    sprintf($strFormatOpt, $strName, $strType, $strRequire, $strRequireValue),
                    $strDesc
                );
            }
        }

        // Build result
        $result = ToolBoxInfoResponse::getStrTable($tabData);

        // Return result
        return $result;
    }



}