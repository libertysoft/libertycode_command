<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\command\route\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\route\route\api\RouteInterface;
use liberty_code\command\request_flow\request\library\ConstCommandRequest;
use liberty_code\command\request_flow\request\model\CommandRequest;
use liberty_code\command\route\library\ConstCommandRoute;
use liberty_code\command\route\exception\ConfigInvalidFormatException;



class ToolBoxCommandRoute extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods check
	// ******************************************************************************

    /**
     * Check if specified value is valid in accordance to specified type.
     *
     * @param mixed $value
     * @param string $strTypeValue = ConstCommandRoute::TAB_CONFIG_SOURCE_ARGUMENT_TYPE_VALUE_STRING
     * @return boolean
     */
    protected static function checkTypeValueMatches($value, $strTypeValue = ConstCommandRoute::TAB_CONFIG_SOURCE_ARGUMENT_TYPE_VALUE_STRING)
    {
        // Init var
        $result = false;

        // Check value type
        switch($strTypeValue)
        {
            case ConstCommandRoute::TAB_CONFIG_SOURCE_ARGUMENT_TYPE_VALUE_STRING:
                $result = is_string($value);
                break;

            case ConstCommandRoute::TAB_CONFIG_SOURCE_ARGUMENT_TYPE_VALUE_NUMERIC:
                $result = is_numeric($value);
                break;

            case ConstCommandRoute::TAB_CONFIG_SOURCE_ARGUMENT_TYPE_VALUE_INTEGER:
                $result = (
                    (is_string($value) && ctype_digit($value)) ||
                    is_int($value)
                );
                break;

            case ConstCommandRoute::TAB_CONFIG_SOURCE_ARGUMENT_TYPE_VALUE_BOOLEAN:
                $result =
                    is_bool($value) ||
                    (is_string($value) && ((strtolower($value) == 'false') || (strtolower($value) == 'true'))) ||
                    (is_numeric($value) && ((intval($value) == 0) || (intval($value) == 1)));
                break;
        }

        // Return result
        return $result;
    }



    /**
     * Check if specified value is valid in accordance to specified enumeration.
     *
     * @param mixed $value
     * @param array $tabEnumValue
     * @param string $strTypeValue = ConstCommandRoute::TAB_CONFIG_SOURCE_ARGUMENT_TYPE_VALUE_STRING
     * @return boolean
     */
    protected static function checkEnumValueMatches(
        $value,
        array $tabEnumValue,
        $strTypeValue = ConstCommandRoute::TAB_CONFIG_SOURCE_ARGUMENT_TYPE_VALUE_STRING
    )
    {
        // Init var
        $valueFormat = static::getTypeValueFormat($value, $strTypeValue);
        $tabEnumValue = array_map(
            function($enumValue) use ($strTypeValue)
            {
                return static::getTypeValueFormat($enumValue, $strTypeValue);
            },
            $tabEnumValue
        );
        $result = (
            (count($tabEnumValue) == 0) ||
            (
                (!is_null($value)) &&
                in_array($valueFormat, $tabEnumValue)
            )
        );

        // Return result
        return $result;
    }



    /**
     * Check if specified request argument array is valid with specified configuration.
     *
	 * @param array $tabRequestSrcArg
	 * @param array $tabRouteConfigArg
     * @return boolean
     */
    public static function checkArgMatches(array $tabRequestSrcArg, array $tabRouteConfigArg)
    {
        // Init var
		$result = (count($tabRequestSrcArg) >= count($tabRouteConfigArg));

        // Run all arguments
        for($intCpt = 0; $result && ($intCpt < count($tabRouteConfigArg)); $intCpt++)
        {
            // Get info
            $tabArgInfo = $tabRouteConfigArg[$intCpt];
            $argValue = $tabRequestSrcArg[$intCpt];

            // Check value type
            $result =
                // Check value type valid
                (
                    isset($tabArgInfo[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_TYPE_VALUE]) ?
                        static::checkTypeValueMatches(
                            $argValue,
                            $tabArgInfo[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_TYPE_VALUE]
                        ) :
                        static::checkTypeValueMatches($argValue)
                ) &&

                // Check value enumeration valid
                (
                    (!isset($tabArgInfo[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_ENUM_VALUE])) ||
                    (
                        isset($tabArgInfo[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_TYPE_VALUE]) ?
                            static::checkEnumValueMatches(
                                $argValue,
                                $tabArgInfo[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_ENUM_VALUE],
                                $tabArgInfo[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_TYPE_VALUE]
                            ) :
                            static::checkEnumValueMatches(
                                $argValue,
                                $tabArgInfo[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_ENUM_VALUE]
                            )
                    )
                );
        }

        // Return result
        return $result;
    }



    /**
     * Check if specified request option array is valid with specified configuration.
     *
     * @param array $tabRequestSrcOpt
     * @param array $tabRouteConfigOpt
     * @return boolean
     */
    public static function checkOptMatches(array $tabRequestSrcOpt, array $tabRouteConfigOpt)
    {
        // Init var
        $result = true;

        // Run all options, if required
        if((count($tabRouteConfigOpt) > 0))
        {
            for($intCpt = 0; $result && ($intCpt < count($tabRouteConfigOpt)); $intCpt++)
            {
                // Get info
                $tabOptInfo = $tabRouteConfigOpt[$intCpt];

                // Run all option names to find potential name used in request
                $strName = null;
                $boolDuplicate = false;
                for($intCpt2 = 0; (!$boolDuplicate) && ($intCpt2 < count($tabOptInfo[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_NAME])); $intCpt2++)
                {
                    // Get info
                    $strOptName = $tabOptInfo[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_NAME][$intCpt2];

                    // Register name, if required
                    if(array_key_exists($strOptName, $tabRequestSrcOpt))
                    {
                        // Register name, if not already done (duplication not authorized)
                        $boolDuplicate = (!is_null($strName));
                        if(!$boolDuplicate)
                        {
                            $strName = $strOptName;
                        }
                    }
                }

                $result =
                    // Duplication not authorized
                    (!$boolDuplicate) &&

                    // Check required valid
                    (
                        (intval($tabOptInfo[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_REQUIRED]) == 0) ||
                        ($strName != null) // Check option found in request
                    ) &&

                    // Check required value valid
                    (
                        (intval($tabOptInfo[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_REQUIRED_VALUE]) == 0) ||
                        (!array_key_exists($strName, $tabRequestSrcOpt)) ||
                        (!is_null($tabRequestSrcOpt[$strName]))
                    ) &&

                    // Check value type valid
                    (
                        (!array_key_exists($strName, $tabRequestSrcOpt)) ||
                        (is_null($tabRequestSrcOpt[$strName])) ||
                        (
                            isset($tabOptInfo[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_TYPE_VALUE]) ?
                                static::checkTypeValueMatches($tabRequestSrcOpt[$strName], $tabOptInfo[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_TYPE_VALUE]) :
                                static::checkTypeValueMatches($tabRequestSrcOpt[$strName])
                        )
                    ) &&

                    // Check value enumeration valid
                    (
                        (!array_key_exists($strName, $tabRequestSrcOpt)) ||
                        (is_null($tabRequestSrcOpt[$strName])) ||
                        (!isset($tabOptInfo[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_ENUM_VALUE])) ||
                        (
                            isset($tabOptInfo[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_TYPE_VALUE]) ?
                                static::checkEnumValueMatches(
                                    $tabRequestSrcOpt[$strName],
                                    $tabOptInfo[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_ENUM_VALUE],
                                    $tabOptInfo[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_TYPE_VALUE]
                                ) :
                                static::checkEnumValueMatches(
                                    $tabRequestSrcOpt[$strName],
                                    $tabOptInfo[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_ENUM_VALUE]
                                )
                        )
                    );
            }
        }


        // Return result
        return $result;
    }



    /**
     * Check if specified request source is composed
     * Route provided with additional information:
     * - Argument array
     * - Option array
     *
     * @param string $strSrc
     * @return boolean
     */
    public static function checkRequestSrcIsComposed($strSrc)
    {
        // Init var
        $result = false;

        // Check source is composed, if required
        if(is_string($strSrc))
        {
            $tabSrc = json_decode($strSrc, true);
            $result =
                is_array($tabSrc) &&
                array_key_exists(ConstCommandRequest::CONF_ROUTE_SOURCE_KEY_ROUTE, $tabSrc) &&
                isset($tabSrc[ConstCommandRequest::CONF_ROUTE_SOURCE_KEY_ARGUMENT]) &&
                is_array($tabSrc[ConstCommandRequest::CONF_ROUTE_SOURCE_KEY_ARGUMENT]) &&
                isset($tabSrc[ConstCommandRequest::CONF_ROUTE_SOURCE_KEY_OPTION]) &&
                is_array($tabSrc[ConstCommandRequest::CONF_ROUTE_SOURCE_KEY_OPTION]);
        }

        // Return result
        return $result;
    }
	




	// Methods getters
    // ******************************************************************************

	/**
     * Get string route from specified request source.
     * 
	 * @param string $strSrc
	 * @return null|string
     */
    public static function getStrRouteFromRequestSrc($strSrc)
    {
		// Init var
        $result = null;

        // Register route, if source valid
        if(is_string($strSrc))
        {
            // Init var
            $result = $strSrc;

            // Register route in composed case, if required
            if(static::checkRequestSrcIsComposed($strSrc))
            {
                $tabSrc = json_decode($strSrc, true);
                $result = $tabSrc[ConstCommandRequest::CONF_ROUTE_SOURCE_KEY_ROUTE];
            }
        }
		
		// Return result
		return $result;
    }



    /**
     * Get index array of arguments from specified request source.
     *
     * @param string $strSrc
     * @return null|array
     */
    public static function getTabArgFromRequestSrc($strSrc)
    {
        // Init var
        $result = null;

        // Register route, if source valid (composed)
        if(static::checkRequestSrcIsComposed($strSrc))
        {
            $tabSrc = json_decode($strSrc, true);
            $result = $tabSrc[ConstCommandRequest::CONF_ROUTE_SOURCE_KEY_ARGUMENT];
        }

        // Return result
        return $result;
    }



    /**
     * Get associative array of options from specified request source.
     *
     * @param string $strSrc
     * @return null|array
     */
    public static function getTabOptFromRequestSrc($strSrc)
    {
        // Init var
        $result = null;

        // Register route, if source valid (composed)
        if(static::checkRequestSrcIsComposed($strSrc))
        {
            $tabSrc = json_decode($strSrc, true);
            $result = $tabSrc[ConstCommandRequest::CONF_ROUTE_SOURCE_KEY_OPTION];
        }

        // Return result
        return $result;
    }



    /**
     * Get index array of source arguments from specified route configuration.
     *
     * @param array $tabConfig
     * @param string $strType = ConstCommandRoute::TAB_CONFIG_SOURCE_ARGUMENT_TYPE_ARGUMENT
     * @return null|array
     */
    protected static function getTabSrcArgFromRouteConfig(array $tabConfig, $strType = ConstCommandRoute::TAB_CONFIG_SOURCE_ARGUMENT_TYPE_ARGUMENT)
    {
        // Init var
        $result = null;

        // Process if configuration valid
        if(isset($tabConfig[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT]))
        {
            // Init var
            $result = array();

            // Run all arguments
            foreach($tabConfig[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT] as $tabConfigInfo)
            {
                // Register argument, if required
                if(
                    // Only argument types required
                    (
                        $strType == ConstCommandRoute::TAB_CONFIG_SOURCE_ARGUMENT_TYPE_ARGUMENT &&
                        $tabConfigInfo[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_TYPE] == ConstCommandRoute::TAB_CONFIG_SOURCE_ARGUMENT_TYPE_ARGUMENT
                    ) ||
                    // Only option types required
                    (
                        $strType == ConstCommandRoute::TAB_CONFIG_SOURCE_ARGUMENT_TYPE_OPTION &&
                        $tabConfigInfo[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_TYPE] == ConstCommandRoute::TAB_CONFIG_SOURCE_ARGUMENT_TYPE_OPTION
                    )
                )
                {
                        $result[] = $tabConfigInfo;
                }
            }
        }

        // Return result
        return $result;
    }



	/**
     * Get index array of arguments from specified route configuration.
     *
     * @param array $tabConfig
     * @return null|array
     */
    public static function getTabArgFromRouteConfig(array $tabConfig)
    {
        // Return result
        return static::getTabSrcArgFromRouteConfig($tabConfig, ConstCommandRoute::TAB_CONFIG_SOURCE_ARGUMENT_TYPE_ARGUMENT);
    }



    /**
     * Get index array of options from specified route configuration.
     *
     * @param array $tabConfig
     * @return null|array
     */
    public static function getTabOptFromRouteConfig(array $tabConfig)
    {
        // Return result
        return static::getTabSrcArgFromRouteConfig($tabConfig, ConstCommandRoute::TAB_CONFIG_SOURCE_ARGUMENT_TYPE_OPTION);
    }



    /**
     * Get formatted value from specified value in accordance to specified type.
     *
     * @param mixed $value
     * @param string $strTypeValue = ConstCommandRoute::TAB_CONFIG_SOURCE_ARGUMENT_TYPE_VALUE_STRING
     * @return null|mixed
     */
    protected static function getTypeValueFormat($value, $strTypeValue = ConstCommandRoute::TAB_CONFIG_SOURCE_ARGUMENT_TYPE_VALUE_STRING)
    {
        // Init var
        $result = null;

        // Register formatted value, if format valid
        if(static::checkTypeValueMatches($value, $strTypeValue))
        {
            switch($strTypeValue)
            {
                case ConstCommandRoute::TAB_CONFIG_SOURCE_ARGUMENT_TYPE_VALUE_STRING:
                    $result = strval($value);
                    break;

                case ConstCommandRoute::TAB_CONFIG_SOURCE_ARGUMENT_TYPE_VALUE_NUMERIC:
                    $result = floatval($value);
                    break;

                case ConstCommandRoute::TAB_CONFIG_SOURCE_ARGUMENT_TYPE_VALUE_INTEGER:
                    $result = intval($value);
                    break;

                case ConstCommandRoute::TAB_CONFIG_SOURCE_ARGUMENT_TYPE_VALUE_BOOLEAN:
                    $result = (
                        is_bool($value) ?
                        $value :
                        (
                            is_numeric($value) ?
                            (intval($value) != 0) :
                            (strtolower($value) == 'true')
                        )
                    );
                    break;
            }
        }

        // Return result
        return $result;
    }



    /**
     * Get specified argument from specified request, formatted with specified route.
     * Default value returned if argument not found, or argument not have valid type value.
     *
     * @param string $strName
     * @param CommandRequest $objRequest
     * @param RouteInterface $objRoute
     * @param mixed $default = null
     * @return mixed
     * @throws ConfigInvalidFormatException
     */
    public static function getArgValue($strName, CommandRequest $objRequest, RouteInterface $objRoute, $default = null)
    {
        // Set check route configuration
        ConfigInvalidFormatException::setCheck($objRoute->getTabConfig());

        // Init var
        $result = $default;
        $tabConfigArg = static::getTabArgFromRouteConfig($objRoute->getTabConfig());

        // Run all arguments to find selected argument
        $tabArgInfo = null;
        for($intCpt = 0; is_null($tabArgInfo) && ($intCpt < count($tabConfigArg)); $intCpt++)
        {
            // Get info
            $tabConfigArgInfo = $tabConfigArg[$intCpt];

            // Get argument info, if found
            if(
                ($tabConfigArgInfo[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_NAME] == $strName) &&
                $objRequest->checkArgExists($intCpt)
            )
            {
                $tabArgInfo = $tabConfigArgInfo;
            }
        }

        // Check argument found (in request and associated route configuration)
        $intCpt--;
        if(!is_null($tabArgInfo))
        {
            // Get value
            $value =
                isset($tabArgInfo[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_TYPE_VALUE]) ?
                static::getTypeValueFormat($objRequest->getArg($intCpt), $tabArgInfo[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_TYPE_VALUE]) :
                static::getTypeValueFormat($objRequest->getArg($intCpt));

            // Register formatted value, if format valid
            if(!is_null($value))
            {
                $result = $value;
            }
        }

        // Return result
        return $result;
    }



    /**
     * Get specified option from specified request, formatted with specified route.
     * Default value returned if option not found, or option not have valid type value.
     *
     * @param string $strName
     * @param CommandRequest $objRequest
     * @param RouteInterface $objRoute
     * @param mixed $default = false
     * @return mixed
     * @throws ConfigInvalidFormatException
     */
    public static function getOptValue($strName, CommandRequest $objRequest, RouteInterface $objRoute, $default = false)
    {
        // Set check route configuration
        ConfigInvalidFormatException::setCheck($objRoute->getTabConfig());

        // Init var
        $result = $default;
        $tabConfigOpt = static::getTabOptFromRouteConfig($objRoute->getTabConfig());

        // Run all options to find selected argument
        $tabOptInfo = null;
        for($intCpt = 0; is_null($tabOptInfo) && ($intCpt < count($tabConfigOpt)); $intCpt++)
        {
            // Get info
            $tabConfigOptInfo = $tabConfigOpt[$intCpt];

            // Run all names to find selected name
            for($intCpt2 = 0; is_null($tabOptInfo) && ($intCpt2 < count($tabConfigOptInfo[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_NAME])); $intCpt2++)
            {
                // Get info
                $strOptName = $tabConfigOptInfo[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_NAME][$intCpt2];

                // Get option info, if found
                if($strOptName == $strName)
                {
                    $tabOptInfo = $tabConfigOptInfo;
                }
            }
        }

        // Check option found (in route configuration)
        if(!is_null($tabOptInfo))
        {
            // Get name used in request
            $strOptName = null;
            if($objRequest->checkOptExists($strName)) // Try to get specified name
            {
                $strOptName = $strName;
            }
            else // Try to get find name
            {
                // Run all names
                for($intCpt = 0; is_null($strOptName) && ($intCpt < count($tabOptInfo[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_NAME])); $intCpt++)
                {
                    // Get info
                    $strConfigOptName = $tabOptInfo[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_NAME][$intCpt];

                    // Get option name, if found
                    if($objRequest->checkOptExists($strConfigOptName))
                    {
                        $strOptName = $strConfigOptName;
                    }
                }
            }

            // Check name found
            if(!is_null($strOptName))
            {
                $value = $objRequest->getOpt($strOptName);

                // Register null value, if authorized
                if(is_null($value) && (intval($tabOptInfo[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_REQUIRED_VALUE]) == 0))
                {
                    $result = $value;
                }
                // Register formatted value, if format valid
                else
                {
                    $value =
                        isset($tabOptInfo[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_TYPE_VALUE]) ?
                        static::getTypeValueFormat($value, $tabOptInfo[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_TYPE_VALUE]) :
                        static::getTypeValueFormat($value);

                    if(!is_null($value))
                    {
                        $result = $value;
                    }
                }
            }
        }

        // Return result
        return $result;
    }



}