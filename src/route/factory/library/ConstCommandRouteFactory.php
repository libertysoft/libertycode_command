<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\command\route\factory\library;



class ConstCommandRouteFactory
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Type configuration
    const CONFIG_TYPE_COMMAND_PATTERN = 'command_pattern';
    const CONFIG_TYPE_COMMAND_PARAM = 'command_param';
    const CONFIG_TYPE_COMMAND_FIX = 'command_fix';
    const CONFIG_TYPE_COMMAND_SEPARATOR = 'command_separator';
}