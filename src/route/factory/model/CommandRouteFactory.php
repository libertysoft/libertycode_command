<?php
/**
 * Description :
 * This class allows to define command route factory class.
 * Command route factory allows to provide and hydrate command route instance.
 *
 * Command route factory uses the following specified configuration, to get and hydrate route:
 * [
 *     -> Configuration key(optional):
 *         "route key OR source (if source not found on command pattern route configuration)"
 *     type(required): "command_pattern",
 *     Command pattern route configuration (@see CommandPatternRoute )
 *
 *     OR
 *
 *     -> Configuration key(optional):
 *         "route key OR source (if source not found on command param route configuration)"
 *     type(optional): "command_param",
 *     Command param route configuration (@see CommandParamRoute )
 *
 *     OR
 *
 *     -> Configuration key(optional):
 *         "route key OR source (if source not found on command fix route configuration)"
 *     type(required): "command_fix",
 *     Command fix route configuration (@see CommandFixRoute )
 *
 *     OR
 *
 *     -> Configuration key(optional): "route key"
 *     type(required): "command_separator",
 *     Command separator route configuration (@see CommandSeparatorRoute )
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\Command\route\factory\model;

use liberty_code\route\route\factory\model\DefaultRouteFactory;

use liberty_code\route\route\library\ConstRoute;
use liberty_code\route\route\pattern\library\ConstPatternRoute;
use liberty_code\route\route\param\library\ConstParamRoute;
use liberty_code\route\route\fix\library\ConstFixRoute;
use liberty_code\route\route\factory\library\ConstRouteFactory;
use liberty_code\command\route\pattern\model\CommandPatternRoute;
use liberty_code\command\route\param\model\CommandParamRoute;
use liberty_code\command\route\fix\model\CommandFixRoute;
use liberty_code\command\route\separator\model\CommandSeparatorRoute;
use liberty_code\command\route\factory\library\ConstCommandRouteFactory;



class CommandRouteFactory extends DefaultRouteFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods getters
	// ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabConfigFormat(array $tabConfig, $strConfigKey = null)
    {
        // Init var
        $result = parent::getTabConfigFormat($tabConfig, $strConfigKey);

        // Format configuration, if required
        if(!is_null($strConfigKey))
        {
            // Get source configuration, from type
            $strConfigKeySource = null;
            $strConfigType = (
                array_key_exists(ConstRouteFactory::TAB_CONFIG_KEY_TYPE, $tabConfig) ?
                    $tabConfig[ConstRouteFactory::TAB_CONFIG_KEY_TYPE] :
                    null
            );
            switch($strConfigType)
            {
                case ConstCommandRouteFactory::CONFIG_TYPE_COMMAND_PATTERN:
                    $strConfigKeySource = ConstPatternRoute::TAB_CONFIG_KEY_SOURCE;
                    break;

                case null:
                case ConstCommandRouteFactory::CONFIG_TYPE_COMMAND_PARAM:
                    $strConfigKeySource = ConstParamRoute::TAB_CONFIG_KEY_SOURCE;
                    break;

                case ConstCommandRouteFactory::CONFIG_TYPE_COMMAND_FIX:
                    $strConfigKeySource = ConstFixRoute::TAB_CONFIG_KEY_SOURCE;
                    break;
            }

            // Add configured key as source, if required
            if(
                (!is_null($strConfigKeySource)) &&
                (!array_key_exists($strConfigKeySource, $result))
            )
            {
                $result[$strConfigKeySource] = $strConfigKey;
            }
            // Add configured key as route key, if required
            else if(!array_key_exists(ConstRoute::TAB_CONFIG_KEY_KEY, $result))
            {
                $result[ConstRoute::TAB_CONFIG_KEY_KEY] = $strConfigKey;
            }
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getStrRouteClassPathFromType($strConfigType)
    {
        // Init var
        $result = null;

        // Get class path of route, from type
        switch($strConfigType)
        {
            case ConstCommandRouteFactory::CONFIG_TYPE_COMMAND_PATTERN:
                $result = CommandPatternRoute::class;
                break;

            case null:
            case ConstCommandRouteFactory::CONFIG_TYPE_COMMAND_PARAM:
                $result = CommandParamRoute::class;
                break;

            case ConstCommandRouteFactory::CONFIG_TYPE_COMMAND_FIX:
                $result = CommandFixRoute::class;
                break;

            case ConstCommandRouteFactory::CONFIG_TYPE_COMMAND_SEPARATOR:
                $result = CommandSeparatorRoute::class;
                break;
        }

        // Return result
        return $result;
    }

	
	
}