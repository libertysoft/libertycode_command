<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\command\route\exception;

use liberty_code\command\route\library\ConstCommandRoute;



class ConfigInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstCommandRoute::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 50, "...")
        );
	}





	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified enumeration is valid.
     *
     * @param array $tabEnumValue
     * @param string $strTypeValue = null
     * @return boolean
     */
    protected static function checkEnumIsArg(array $tabEnumValue, $strTypeValue = null)
    {
        // Init var
        $result = is_array($tabEnumValue);

        // Run each enumeration value
        for($intCpt = 0; ($intCpt < count($tabEnumValue)) && $result; $intCpt++)
        {
            $enumValue = $tabEnumValue[$intCpt];
            $result = (
                // Check valid string
                (
                    (
                        is_null($strTypeValue) ||
                        ($strTypeValue == ConstCommandRoute::TAB_CONFIG_SOURCE_ARGUMENT_TYPE_VALUE_STRING)
                    ) &&
                    is_string($enumValue)
                ) ||

                // Check valid numeric
                (
                    ($strTypeValue == ConstCommandRoute::TAB_CONFIG_SOURCE_ARGUMENT_TYPE_VALUE_NUMERIC) &&
                    is_numeric($enumValue)
                ) ||

                // Check valid integer
                (
                    ($strTypeValue == ConstCommandRoute::TAB_CONFIG_SOURCE_ARGUMENT_TYPE_VALUE_INTEGER) &&
                    is_int($enumValue)
                ) ||

                // Check valid boolean
                (
                    ($strTypeValue == ConstCommandRoute::TAB_CONFIG_SOURCE_ARGUMENT_TYPE_VALUE_BOOLEAN) &&
                    (
                        is_bool($enumValue) ||
                        (is_string($enumValue) && ((strtolower($enumValue) == 'false') || (strtolower($enumValue) == 'true'))) ||
                        (is_numeric($enumValue) && ((intval($enumValue) == 0) || (intval($enumValue) == 1)))
                    )

                )
            );
        }

        // Return result
        return $result;
    }



    /**
     * Check if specified config is an argument.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsArg($config)
    {
        // Init var
        $result =
            is_array($config) &&

            // Check valid type
            isset($config[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_TYPE]) &&
            is_string($config[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_TYPE]) &&
            ($config[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_TYPE] == ConstCommandRoute::TAB_CONFIG_SOURCE_ARGUMENT_TYPE_ARGUMENT) &&

            // Check valid name
            isset($config[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_NAME]) &&
            is_string($config[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_NAME]) &&
            (trim($config[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_NAME]) != '') &&
            ctype_alnum(str_replace(['_', '-'], '', $config[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_NAME])) &&

            // Check valid description
            (
                (!isset($config[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_DESCRIPTION])) ||
                (
                    is_string($config[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_DESCRIPTION]) &&
                    (trim($config[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_DESCRIPTION]) != '')
                )
            ) &&

            // Check valid value type
            (
                (!isset($config[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_TYPE_VALUE])) ||
                (
                    is_string($config[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_TYPE_VALUE]) &&
                    (
                        ($config[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_TYPE_VALUE] == ConstCommandRoute::TAB_CONFIG_SOURCE_ARGUMENT_TYPE_VALUE_STRING) ||
                        ($config[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_TYPE_VALUE] == ConstCommandRoute::TAB_CONFIG_SOURCE_ARGUMENT_TYPE_VALUE_NUMERIC) ||
                        ($config[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_TYPE_VALUE] == ConstCommandRoute::TAB_CONFIG_SOURCE_ARGUMENT_TYPE_VALUE_INTEGER) ||
                        ($config[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_TYPE_VALUE] == ConstCommandRoute::TAB_CONFIG_SOURCE_ARGUMENT_TYPE_VALUE_BOOLEAN)
                    )
                )
            ) &&

            // Check valid value enumeration
            (
                (!isset($config[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_ENUM_VALUE])) ||
                (
                    is_array($config[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_ENUM_VALUE]) &&
                    (
                        isset($config[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_TYPE_VALUE]) ?
                            static::checkEnumIsArg(
                                $config[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_ENUM_VALUE],
                                $config[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_TYPE_VALUE]
                            ) :
                            static::checkEnumIsArg($config[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_ENUM_VALUE])
                    )
                )
            );

        // Return result
        return $result;
    }



    /**
     * Check if specified config is an option.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsOpt($config)
    {
        // Init var
        $result =
            is_array($config) &&

            // Check valid type
            isset($config[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_TYPE]) &&
            is_string($config[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_TYPE]) &&
            ($config[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_TYPE] == ConstCommandRoute::TAB_CONFIG_SOURCE_ARGUMENT_TYPE_OPTION) &&

            // Check valid name
            isset($config[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_NAME]) &&
            is_array($config[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_NAME]) &&
            (count($config[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_NAME]) > 0) &&
            // Check all names are valid string (count array filtered on valid string names, equals count array names)
            (count($config[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_NAME]) == count(array_filter(
                $config[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_NAME],
                function($value){
                    return (
                        is_string($value) &&
                        (trim($value) != '') &&
                        ctype_alnum(str_replace(['_', '-'], '', $value))
                    );
                }
            ))) &&

            // Check valid description
            (
                (!isset($config[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_DESCRIPTION])) ||
                (
                    is_string($config[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_DESCRIPTION]) &&
                    (trim($config[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_DESCRIPTION]) != '')
                )
            ) &&

            // Check valid required
            isset($config[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_REQUIRED]) &&
            (
                // Check is boolean or numeric (0 = false, 1 = true)
                is_bool($config[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_REQUIRED]) ||
                is_int($config[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_REQUIRED]) ||
                (
                    is_string($config[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_REQUIRED]) &&
                    ctype_digit($config[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_REQUIRED])
                )
            ) &&

            // Check valid required value
            isset($config[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_REQUIRED_VALUE]) &&
            (
                // Check is boolean or numeric (0 = false, 1 = true)
                is_bool($config[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_REQUIRED_VALUE]) ||
                is_int($config[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_REQUIRED_VALUE]) ||
                (
                    is_string($config[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_REQUIRED_VALUE]) &&
                    ctype_digit($config[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_REQUIRED_VALUE])
                )
            ) &&

            // Check valid value type
            (
                (!isset($config[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_TYPE_VALUE])) ||
                (
                    is_string($config[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_TYPE_VALUE]) &&
                    (
                        ($config[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_TYPE_VALUE] == ConstCommandRoute::TAB_CONFIG_SOURCE_ARGUMENT_TYPE_VALUE_STRING) ||
                        ($config[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_TYPE_VALUE] == ConstCommandRoute::TAB_CONFIG_SOURCE_ARGUMENT_TYPE_VALUE_NUMERIC) ||
                        ($config[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_TYPE_VALUE] == ConstCommandRoute::TAB_CONFIG_SOURCE_ARGUMENT_TYPE_VALUE_INTEGER) ||
                        ($config[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_TYPE_VALUE] == ConstCommandRoute::TAB_CONFIG_SOURCE_ARGUMENT_TYPE_VALUE_BOOLEAN)
                    )
                )
            ) &&

            // Check valid value enumeration
            (
                (!isset($config[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_ENUM_VALUE])) ||
                (
                    is_array($config[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_ENUM_VALUE]) &&
                    (
                        isset($config[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_TYPE_VALUE]) ?
                            static::checkEnumIsArg(
                                $config[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_ENUM_VALUE],
                                $config[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_TYPE_VALUE]
                            ) :
                            static::checkEnumIsArg($config[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_ENUM_VALUE])
                    )
                )
            );

        // Return result
        return $result;
    }



    /**
     * Check if specified config has valid source argument format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkSrcArgConfigIsValid($config)
    {
        // Init var
        $result = false;
        $tabName = array();

        // Run all arguments, if valid
        if(is_array($config))
        {
            for($intCpt = 0; ($result || ($intCpt == 0)) && ($intCpt < count($config)); $intCpt++)
            {
                // Init var
                $tabArgName = array();

                // Get list of names, if valid
                if(isset($config[$intCpt]))
                {
                    $configArg = $config[$intCpt];

                    // Case: argument
                    if (static::checkConfigIsArg($configArg))
                    {
                        $tabArgName[] = trim($configArg[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_NAME]);
                    }
                    // Case: option
                    else if (static::checkConfigIsOpt($configArg))
                    {
                        foreach ($configArg[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT_NAME] as $strName) {
                            $tabArgName[] = trim($strName);
                        }
                    }
                }

                // Check valid argument
                $tabName = array_merge($tabName, $tabArgName);
                $result =
                    (count($tabArgName) > 0) && // Check at least 1 name found (Is argument or option)
                    (count(array_unique($tabName)) == count($tabName)); // Check each name is unique
            }
        }

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($config)
    {
		// Init var
		$result = 
			// Check valid array
			(is_array($config) && (count($config) > 0)) &&

            // Check valid description, if required
            (
                (!isset($config[ConstCommandRoute::TAB_CONFIG_KEY_DESCRIPTION])) ||
                (
                    is_string($config[ConstCommandRoute::TAB_CONFIG_KEY_DESCRIPTION]) &&
                    (trim($config[ConstCommandRoute::TAB_CONFIG_KEY_DESCRIPTION]) != '')
                )
            ) &&

            // Check valid source argument config, if required
            (
                (!isset($config[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT])) ||
                static::checkSrcArgConfigIsValid($config[ConstCommandRoute::TAB_CONFIG_KEY_SOURCE_ARGUMENT])
            );

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}