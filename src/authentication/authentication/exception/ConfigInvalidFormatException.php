<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\command\authentication\authentication\exception;

use liberty_code\command\authentication\authentication\library\ConstCommandAuthentication;



class ConfigInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstCommandAuthentication::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init index array of data check function
        $checkTabDataIsValid = function(array $tabData)
        {
            $result = true;
            $tabData = array_values($tabData);

            // Check each data is valid
            for($intCpt = 0; ($intCpt < count($tabData)) && $result; $intCpt++)
            {
                $data = $tabData[$intCpt];
                $result = (
                    // Check valid argument type
                    (
                        (!array_key_exists(ConstCommandAuthentication::TAB_CONFIG_KEY_ARG_TYPE, $data)) ||
                        (
                            is_string($data[ConstCommandAuthentication::TAB_CONFIG_KEY_ARG_TYPE]) &&
                            in_array(
                                $data[ConstCommandAuthentication::TAB_CONFIG_KEY_ARG_TYPE],
                                ConstCommandAuthentication::getTabConfigArgType()
                            )
                        )
                    ) &&

                    // Check valid argument key
                    isset($data[ConstCommandAuthentication::TAB_CONFIG_KEY_ARG_KEY]) &&
                    (
                        // Check argument key valid integer index, in case of type argument
                        (
                            array_key_exists(ConstCommandAuthentication::TAB_CONFIG_KEY_ARG_TYPE, $data) &&
                            ($data[ConstCommandAuthentication::TAB_CONFIG_KEY_ARG_TYPE] ==
                                ConstCommandAuthentication::CONFIG_ARG_TYPE_ARGUMENT) &&
                            is_int($data[ConstCommandAuthentication::TAB_CONFIG_KEY_ARG_KEY]) &&
                            ($data[ConstCommandAuthentication::TAB_CONFIG_KEY_ARG_KEY] >= 0)
                        ) ||
                        // Check argument key valid string key, in case of type option
                        (
                            (
                                (!array_key_exists(ConstCommandAuthentication::TAB_CONFIG_KEY_ARG_TYPE, $data)) ||
                                ($data[ConstCommandAuthentication::TAB_CONFIG_KEY_ARG_TYPE] ==
                                    ConstCommandAuthentication::CONFIG_ARG_TYPE_OPTION)
                            ) &&
                            is_string($data[ConstCommandAuthentication::TAB_CONFIG_KEY_ARG_KEY]) &&
                            (trim($data[ConstCommandAuthentication::TAB_CONFIG_KEY_ARG_KEY]) != '')
                        )
                    ) &&

                    // Check valid argument format callable
                    (
                        (!isset($data[ConstCommandAuthentication::TAB_CONFIG_KEY_ARG_FORMAT_CALLABLE])) ||
                        is_callable($data[ConstCommandAuthentication::TAB_CONFIG_KEY_ARG_FORMAT_CALLABLE])
                    ) &&

                    // Check valid data key
                    (
                        (
                            // Check optional data key only if argument key can be substituted (is string)
                            (!array_key_exists(ConstCommandAuthentication::TAB_CONFIG_KEY_DATA_KEY, $data)) &&
                            is_string($data[ConstCommandAuthentication::TAB_CONFIG_KEY_ARG_KEY])
                        ) ||
                        (
                            array_key_exists(ConstCommandAuthentication::TAB_CONFIG_KEY_DATA_KEY, $data) &&
                            is_string($data[ConstCommandAuthentication::TAB_CONFIG_KEY_DATA_KEY]) &&
                            (trim($data[ConstCommandAuthentication::TAB_CONFIG_KEY_DATA_KEY]) != '')
                        )
                    ) &&

                    // Check valid data format callable
                    (
                        (!isset($data[ConstCommandAuthentication::TAB_CONFIG_KEY_DATA_FORMAT_CALLABLE])) ||
                        is_callable($data[ConstCommandAuthentication::TAB_CONFIG_KEY_DATA_FORMAT_CALLABLE])
                    )
                );
            }

            return $result;
        };

        // Init var
        $result =
            // Check valid identification data
            isset($config[ConstCommandAuthentication::TAB_CONFIG_KEY_ID_DATA]) &&
            is_array($config[ConstCommandAuthentication::TAB_CONFIG_KEY_ID_DATA]) &&
            (count($config[ConstCommandAuthentication::TAB_CONFIG_KEY_ID_DATA]) > 0) &&
            $checkTabDataIsValid($config[ConstCommandAuthentication::TAB_CONFIG_KEY_ID_DATA]) &&

            // Check valid authentication data
            (
                (!isset($config[ConstCommandAuthentication::TAB_CONFIG_KEY_AUTH_DATA])) ||
                (
                    is_array($config[ConstCommandAuthentication::TAB_CONFIG_KEY_AUTH_DATA]) &&
                    $checkTabDataIsValid($config[ConstCommandAuthentication::TAB_CONFIG_KEY_AUTH_DATA])
                )
            );

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format.
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}