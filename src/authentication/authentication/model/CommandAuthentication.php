<?php
/**
 * This class allows to define command authentication class.
 * Command authentication uses command request arguments,
 * to get all identification and authentication information.
 *
 * Command authentication uses the following specified configuration:
 * [
 *     identification_data(required: At least one identification data must be found): [
 *         // Identification data 1
 *         [
 *             arg_type(optional: got option if not found):
 *                 "string argument type" (@see ConstCommandAuthentication::getTabConfigArgType() ),
 *             arg_key(required):
 *                 integer argument index, if arg_type = argument
 *                 OR
 *                 "string argument key", if arg_type = option,
 *             arg_format_callable(optional):
 *                 Get argument formatted value callback function: mixed function(mixed $value),
 *             data_key(optional if arg_key can be substituted (is string): got arg_key if not found, required else):
 *                 "string data key",
 *             data_format_callable(optional):
 *                 Get data formatted value callback function: mixed function(mixed $value)
 *         ],
 *         ...,
 *         // Identification data N
 *         [...]
 *     ],
 *
 *     authentication_data(optional: got [] if not found): [
 *         // Authentication data 1
 *         [
 *             Same format as identification data (described above)
 *         ],
 *         ...,
 *         // Authentication data N
 *         [...]
 *     ]
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\command\authentication\authentication\model;

use liberty_code\authentication\authentication\model\DefaultAuthentication;

use liberty_code\authentication\authentication\library\ConstAuthentication;
use liberty_code\command\request_flow\request\model\CommandRequest;
use liberty_code\command\authentication\authentication\library\ConstCommandAuthentication;
use liberty_code\command\authentication\authentication\exception\RequestInvalidFormatException;
use liberty_code\command\authentication\authentication\exception\ConfigInvalidFormatException;



/**
 * @method null|CommandRequest getObjRequest() Get command request object.
 * @method void setObjRequest(CommandRequest $objRequest) Set command request object.
 */
class CommandAuthentication extends DefaultAuthentication
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param CommandRequest $objRequest = null
     */
    public function __construct(
        CommandRequest $objRequest = null,
        array $tabConfig = null
    )
    {
        // Call parent constructor
        parent::__construct($tabConfig);

        // Init HTTP request, if required
        if(!is_null($objRequest))
        {
            $this->setObjRequest($objRequest);
        }
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstCommandAuthentication::DATA_KEY_DEFAULT_REQUEST))
        {
            $this->__beanTabData[ConstCommandAuthentication::DATA_KEY_DEFAULT_REQUEST] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstCommandAuthentication::DATA_KEY_DEFAULT_REQUEST
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstCommandAuthentication::DATA_KEY_DEFAULT_REQUEST:
                    RequestInvalidFormatException::setCheck($value);
                    break;

                case ConstAuthentication::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get associative array of authentication data engine,
     * from specified index array of configured data.
     *
     * @param array $tabConfigData
     * @return array
     */
    protected function getTabDataEngine(array $tabConfigData)
    {
        // Init var
        $result = array();
        $objRequest = $this->getObjRequest();

        if(!is_null($objRequest))
        {
            // Run each configured data
            foreach($tabConfigData as $configData)
            {
                // Get info
                $strArgType = (
                isset($configData[ConstCommandAuthentication::TAB_CONFIG_KEY_ARG_TYPE]) ?
                    $configData[ConstCommandAuthentication::TAB_CONFIG_KEY_ARG_TYPE] :
                    ConstCommandAuthentication::CONFIG_ARG_TYPE_OPTION
                );
                $argKey = $configData[ConstCommandAuthentication::TAB_CONFIG_KEY_ARG_KEY];
                $callArgFormat = (
                    isset($configData[ConstCommandAuthentication::TAB_CONFIG_KEY_ARG_FORMAT_CALLABLE]) ?
                        $configData[ConstCommandAuthentication::TAB_CONFIG_KEY_ARG_FORMAT_CALLABLE] :
                        null
                );
                $strKey = (
                    isset($configData[ConstCommandAuthentication::TAB_CONFIG_KEY_DATA_KEY]) ?
                        $configData[ConstCommandAuthentication::TAB_CONFIG_KEY_DATA_KEY] :
                        $argKey
                );
                $callFormat = (
                    isset($configData[ConstCommandAuthentication::TAB_CONFIG_KEY_DATA_FORMAT_CALLABLE]) ?
                        $configData[ConstCommandAuthentication::TAB_CONFIG_KEY_DATA_FORMAT_CALLABLE] :
                        null
                );

                // Get value
                switch($strArgType)
                {
                    case ConstCommandAuthentication::CONFIG_ARG_TYPE_ARGUMENT:
                        $value = $objRequest->getArg($argKey, null);
                        break;

                    case ConstCommandAuthentication::CONFIG_ARG_TYPE_OPTION:
                    default:
                        $value = $objRequest->getOpt($argKey, null);
                        break;
                }

                // Get formatted value
                $value = $this->getArgDataFormat($argKey, $value);
                $value = (is_callable($callArgFormat) ? $callArgFormat($value) : $value);
                $value = (is_callable($callFormat) ? $callFormat($value) : $value);

                // Set result info
                $result[$strKey] = $value;
            }
        }

        // Return result
        return $result;
    }



    /**
     * Get specified argument formatted data.
     * Overwrite it to implement specific format.
     *
     * @param integer|string $key
     * @param mixed $value
     * @return mixed
     */
    protected function getArgDataFormat($key, $value)
    {
        // Format by data key
        switch($key)
        {
            default:
                $result = $value;
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getTabIdDataEngine()
    {
        // Init var
        $tabConfig = $this->getTabAuthConfig();
        $result = $this->getTabDataEngine($tabConfig[ConstCommandAuthentication::TAB_CONFIG_KEY_ID_DATA]);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getTabAuthDataEngine()
    {
        // Init var
        $tabConfig = $this->getTabAuthConfig();
        $result = (
            isset($tabConfig[ConstCommandAuthentication::TAB_CONFIG_KEY_AUTH_DATA]) ?
                $this->getTabDataEngine($tabConfig[ConstCommandAuthentication::TAB_CONFIG_KEY_AUTH_DATA]) :
                array()
        );

        // Return result
        return $result;
    }



}