<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\command\config\exception;

use liberty_code\command\config\library\ConstConfig;



class CmdRequestOptRegexpInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $regexp
     */
	public function __construct($regexp)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
		(
            ConstConfig::EXCEPT_MSG_CMD_REQUEST_OPT_REGEXP_INVALID_FORMAT,
            mb_strimwidth(strval($regexp), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if REGEXP has valid format.
	 * 
     * @param mixed $regexp
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($regexp)
    {
		// Init var
		$result = is_array($regexp); // Check is valid array
		
		// Run all REGEXPs
		for($intCpt = 0; $result && ($intCpt < count($regexp)); $intCpt++)
		{
			// Check REGEXP is valid string
			$result = (is_string($regexp[$intCpt]) && (trim($regexp[$intCpt]) !== ''));
		}
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($regexp) ? serialize($regexp) : $regexp));
		}
		
		// Return result
		return $result;
    }
	
	
	
}