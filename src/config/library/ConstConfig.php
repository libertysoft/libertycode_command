<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\command\config\library;



class ConstConfig
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_CMD_REQUEST_OPT_REGEXP = 'tabCmdRequestOptRegexp';
    const DATA_KEY_CMD_REQUEST_OPT_VALUE_REGEXP = 'tabCmdRequestOptValueRegexp';

    const DATA_DEFAULT_VALUE_CMD_REQUEST_OPT_REGEXP = array(
		'#^\-([A-Za-z])$#', // Short option
		'#^\-\-([A-Za-z0-9_\-]{2,})$#' // Long option
	);
    const DATA_DEFAULT_VALUE_CMD_REQUEST_OPT_VALUE_REGEXP = array(
		'#^\-([A-Za-z])(.+)$#', // Short option
		'#^\-\-([A-Za-z0-9_\-]{2,})=(.+)$#' // Long option
	);
	
	
	
    // Exception message constants
    const EXCEPT_MSG_CMD_REQUEST_OPT_REGEXP_INVALID_FORMAT = 'Following command request option REGEXP "%1$s" invalid! The REGEXP must be an array of string, not empty.';
}