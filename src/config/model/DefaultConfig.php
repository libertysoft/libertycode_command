<?php
/**
 * Description :
 * This class allows to provide configuration for command line context features.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\command\config\model;

use liberty_code\library\bean\model\FixBean;

use liberty_code\command\config\library\ConstConfig;
use liberty_code\command\config\exception\CmdRequestOptRegexpInvalidFormatException;



/**
 * @method array getTabCmdRequestOptRegexp() Get index array of string command request REGEXPs for option (without value).
 * @method array getTabCmdRequestOptValueRegexp() Get index array of string command request REGEXPs for option (with value).
 * @method void setTabCmdRequestOptRegexp(array $tabRegexp) Set index array of string command request REGEXPs for option (without value).
 * @method void setTabCmdRequestOptValueRegexp(array $tabRegexp) Set index array of string command request REGEXPs for option (with value).
 */
class DefaultConfig extends FixBean
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods initialize
	// ******************************************************************************
	
	/**
	 * @inheritdoc
	 */
	protected function beanHydrateDefault()
	{
		if(!$this->beanExists(ConstConfig::DATA_KEY_CMD_REQUEST_OPT_REGEXP))
		{
			$this->beanAdd(ConstConfig::DATA_KEY_CMD_REQUEST_OPT_REGEXP, ConstConfig::DATA_DEFAULT_VALUE_CMD_REQUEST_OPT_REGEXP);
		}

        if(!$this->beanExists(ConstConfig::DATA_KEY_CMD_REQUEST_OPT_VALUE_REGEXP))
        {
            $this->beanAdd(ConstConfig::DATA_KEY_CMD_REQUEST_OPT_VALUE_REGEXP, ConstConfig::DATA_DEFAULT_VALUE_CMD_REQUEST_OPT_VALUE_REGEXP);
        }
	}





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstConfig::DATA_KEY_CMD_REQUEST_OPT_REGEXP,
            ConstConfig::DATA_KEY_CMD_REQUEST_OPT_VALUE_REGEXP,
        );
        $result = in_array($key, $tabKey);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstConfig::DATA_KEY_CMD_REQUEST_OPT_REGEXP:
                    CmdRequestOptRegexpInvalidFormatException::setCheck($value);
                    break;

                case ConstConfig::DATA_KEY_CMD_REQUEST_OPT_VALUE_REGEXP:
                    CmdRequestOptRegexpInvalidFormatException::setCheck($value);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidRemove($key, &$error = null)
    {
        // Return result
        return false;
    }
	
	
	
}