LibertyCode_Command
===================



Description
-----------

Library contains command line components, 
to use features and other libraries components, 
on command line context.

---



Requirement
-----------

- Script language: PHP: version 7 || 8

---



Installation
------------

Several ways are possible:

#### Composer

1. Requirement
    
    It requires composer installation.
    For more information: https://getcomposer.org
    
2. Command: Move in project root path
    
    ```sh
    cd "<project_root_path>"
    ```

3. Command: Installation
    
    ```sh
    php composer.phar require liberty_code/command ["<version>"]
    ```
    
4. Note

    - Include vendor
        
        If project uses composer, 
        vendor must be included:
        
        ```php
        require_once('<project_root_path>/vendor/autoload.php');
        ```
    
    - Configuration
    
        Installation command allows to add, 
        on composer file "<project_root_path>/composer.json",
        following configuration:
        
        ```json
        {
            "require": {
                "liberty_code/command": "<version>"
            }
        }
        ```

#### Include

1. Download
    
    - Download following repository.
    - Put it on repository root path.
    
2. Include source
    
    ```php
    require_once('<repository_root_path>/include/Include.php');
    ```

---



Configuration
-------------

#### Main configuration

- Use following class to configure specific elements
    
    ```php
    use liberty_code\command\config\model\DefaultConfig;
    DefaultConfig::instanceGetDefault()->get|set...();
    ```

- Elements configurables

    - Request options

---



Usage
-----

#### Route

Route allows to interpret specified source, 
from command line request, on command line context, 
to get executable.

_Elements_

- CommandPatternRoute, CommandParamRoute, CommandFixRoute, CommandSeparatorRoute

    Extends route features. 
    Adapt all standard routes, 
    to interpret specified source, 
    from command line request, 
    and get information (summary and help). 
    
- CommandRouteFactory

    Extends DI route factory features. 
    Provides command route instance.
    
#### Request

Request allows to provide route source, 
on request flow process and command line context.

_Elements_

- CommandRequest
    
    Extends request features. 
    Allows to design command line request.
    
_Example_

```php
use liberty_code\command\request_flow\request\model\CommandRequest;
$request = CommandRequest::instanceGetDefault();
echo($request->getStrRouteSrc()); 
// Show "{route:<route>,argument:[<command line arguments...>],option:[<command line options...>]}"
...
```

#### Response

Response allows to provide final send-able content, 
from request flow process, on command line context.

_Elements_

- Response utilities
    
    Information command line response allows 
    to get specific information about context, 
    on command line context.
        Example: response with command lines summary content, 
        response with specific command line help content.

#### Front controller

Front controller allows to design the request flow process, 
on command line context.

_Elements_

- CommandFrontController
    
    Extends front controller features. 
    Uses command line request, to get response.
    
- InfoFrontController
    
    Extends command line front controller features. 
    Allows to manage information command line responses.
    
_Example_

```php
use liberty_code\command\request_flow\front\model\CommandFrontController;
$frontController = new CommandFrontController();
$frontController->setRouter(...);
$frontController->setActiveRequest(...);
...
// Show response object
var_dump($frontController->execute());
...
```

#### Authentication

Authentication allows to design an authentication class,
using command request arguments.

_Elements_

- CommandAuthentication
    
    Extends authentication features. 
    Uses command request arguments, 
    to get all identification and authentication information.
    
_Example_

```php
// Get command authentication
use liberty_code\command\request_flow\request\model\CommandRequest;
$commandAuth = new CommandRequest($request);
$commandAuth->setAuthConfig(...);
...
// Get array of identification data
var_dump($commandAuth->getTabIdData());
...
// Get array of authentication data
var_dump($commandAuth->getTabAuthData());
...
```

---


