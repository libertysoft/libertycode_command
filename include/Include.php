<?php

// Init var
$strRootPath = dirname(__FILE__) . '/..';

// Include class
include($strRootPath . '/src/config/library/ConstConfig.php');
include($strRootPath . '/src/config/exception/CmdRequestOptRegexpInvalidFormatException.php');
include($strRootPath . '/src/config/model/DefaultConfig.php');

include($strRootPath . '/src/route/library/ConstCommandRoute.php');
include($strRootPath . '/src/route/library/ToolBoxCommandRoute.php');
include($strRootPath . '/src/route/library/ToolBoxInfoRoute.php');
include($strRootPath . '/src/route/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/route/pattern/model/CommandPatternRoute.php');
include($strRootPath . '/src/route/param/model/CommandParamRoute.php');
include($strRootPath . '/src/route/fix/model/CommandFixRoute.php');
include($strRootPath . '/src/route/separator/model/CommandSeparatorRoute.php');

include($strRootPath . '/src/route/factory/library/ConstCommandRouteFactory.php');
include($strRootPath . '/src/route/factory/model/CommandRouteFactory.php');

include($strRootPath . '/src/request_flow/request/library/ConstCommandRequest.php');
include($strRootPath . '/src/request_flow/request/exception/RouteNotFoundException.php');
include($strRootPath . '/src/request_flow/request/model/CommandRequest.php');

include($strRootPath . '/src/request_flow/response/info/library/ToolBoxInfoResponse.php');

include($strRootPath . '/src/request_flow/front/library/ConstCommandFrontController.php');
include($strRootPath . '/src/request_flow/front/exception/ActiveRequestInvalidFormatException.php');
include($strRootPath . '/src/request_flow/front/exception/DefaultResponseInvalidFormatException.php');
include($strRootPath . '/src/request_flow/front/model/CommandFrontController.php');

include($strRootPath . '/src/request_flow/front/info/library/ConstInfoFrontController.php');
include($strRootPath . '/src/request_flow/front/info/exception/InfoCmdOptInvalidFormatException.php');
include($strRootPath . '/src/request_flow/front/info/exception/InfoSumOptInvalidFormatException.php');
include($strRootPath . '/src/request_flow/front/info/exception/InfoCmdResponseInvalidFormatException.php');
include($strRootPath . '/src/request_flow/front/info/exception/InfoSumResponseInvalidFormatException.php');
include($strRootPath . '/src/request_flow/front/info/exception/InfoOptNameInvalidFormatException.php');
include($strRootPath . '/src/request_flow/front/info/model/InfoFrontController.php');

include($strRootPath . '/src/authentication/authentication/library/ConstCommandAuthentication.php');
include($strRootPath . '/src/authentication/authentication/exception/RequestInvalidFormatException.php');
include($strRootPath . '/src/authentication/authentication/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/authentication/authentication/model/CommandAuthentication.php');